#include "game_object_view.h"

namespace NPC
{
	GameObjectView::GameObjectView() : game_object_(nullptr)
	{
	}

	GameObjectView::~GameObjectView()
	{
	}

	void GameObjectView::Init(GameObject* GO, sf::Color colour)
	{
		game_object_ = GO;

		rectangle_.setSize(Vector2f(game_object_->bounding_box().width(), game_object_->bounding_box().height()));
		rectangle_.setPosition(Vector2f(game_object_->bounding_box().position().x - game_object_->bounding_box().half_width(), 
			game_object_->bounding_box().position().y - game_object_->bounding_box().half_height()));
		rectangle_.setFillColor(colour);
	}

	void GameObjectView::Update()
	{
		rectangle_.setPosition(Vector2f(game_object_->bounding_box().position().x - game_object_->bounding_box().half_width(),
			game_object_->bounding_box().position().y - game_object_->bounding_box().half_height()));
	}

	void GameObjectView::Render(sf::RenderWindow* window)
	{
		window->draw(rectangle_);
	}

}