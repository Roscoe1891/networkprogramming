#ifndef _GAME_OBJECT_VIEW_H
#define _GAME_OBJECT_VIEW_H

#include "SFML\Graphics.hpp"
#include "GameObjects\game_object.h"

namespace NPC
{
	class GameObjectView
	{
	public:
		GameObjectView();
		~GameObjectView();

		void Init(GameObject* GO, sf::Color colour);
		void Update();
		void Render(sf::RenderWindow* window);

	private:
		GameObject* game_object_;
		sf::RectangleShape rectangle_;

	};
}


#endif

