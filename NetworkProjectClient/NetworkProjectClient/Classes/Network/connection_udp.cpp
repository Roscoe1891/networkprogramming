#include "connection_udp.h"

namespace NPC
{
	ConnectionUDP::ConnectionUDP()
	{
		active_ = false;
	}

	ConnectionUDP::~ConnectionUDP()
	{
	}

	void ConnectionUDP::Init(unsigned short port, sf::IpAddress comm_add, unsigned short comm_port)
	{
		// bind the socket to a port
		if (socket_.bind(port) != sf::Socket::Done)
		{
			// error...
		}

		communication_address_ = comm_add;
		communication_port_ = comm_port;
		active_ = true;

		std::cout << "Listening for address" << communication_address_ << ", port " << communication_port_ << ". Local port " << socket_.getLocalPort() << std::endl;
	}

	void ConnectionUDP::Update(float dt)
	{
		if (timeout_timer_ > UDP_TIMEOUT)
		{
			std::cout << "UDP timeout expired for " << communication_address_ << " " << communication_port_ << std::endl;
			active_ = false;
		}
		else
		{
			timeout_timer_ += dt;
		}
	}

	bool ConnectionUDP::Send(sf::Packet send_packet)
	{
		//std::cout << "Trying to send a packet to: " << communication_address_ << ", port: " << communication_port_ << std::endl;

		sf::IpAddress add = communication_address_;
		unsigned short port = communication_port_;

		bool result = false;
		if (socket_.send(send_packet, add, port) == sf::Socket::NotReady)
		{
			return result;
		}
		else if (socket_.send(send_packet, add, port) != sf::Socket::Done)
		{
			// error...
			return result;
			std::cout << "There was an error sending the packet." << std::endl;
		}
		result = true;
		return result;
	}

	bool ConnectionUDP::Receive(sf::Packet& receive_packet)
	{
		//std::cout << "Trying to receive a packet from: " << communication_address_ << ", port: " << communication_port_ << std::endl;

		sf::IpAddress add = communication_address_;
		unsigned short port = communication_port_;

		bool result = false;
		if (socket_.receive(receive_packet, add, port) == sf::Socket::NotReady)
		{
			return result;
		}
		else if (socket_.receive(receive_packet, add, port) != sf::Socket::Done)
		{
			// error...
			return result;
			std::cout << "There was an error receiving the packet." << std::endl;
		}
		result = true;
		timeout_timer_ = 0;
		return result;
	}

	void ConnectionUDP::SetSocketBlocking(bool new_value)
	{
		socket_.setBlocking(new_value);
	}

	sf::Packet ConnectionUDP::CreatePacket(PacketType type, sf::Uint64 time)
	{
		sf::Packet result;

		switch (type)
		{
		case TIME_STAMP:
			result << time;
			break;
		default:
			break;
		}

		return result;

	}

	void ConnectionUDP::CleanUp()
	{
		socket_.unbind();
	}
}