#include "connection_manager_tcp.h"

namespace NPC
{
	ConnectionManagerTCP::ConnectionManagerTCP()
	{
		host_address_ = sf::IpAddress::None;

		for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
		{
			client_addresses_[i] = sf::IpAddress::None;
		}

	}

	ConnectionManagerTCP::~ConnectionManagerTCP()
	{
	}

	// return value of server connection 'active'
	bool ConnectionManagerTCP::ConnectedToServer()
	{
		return server_connection_.active();
	}

	void ConnectionManagerTCP::DisconnectFromServer()
	{
		// if connection is active
		if (server_connection_.active())
		{
			// inform server
			SendToServer(string("Disconnected"));
			ReceiveFromServer();
			
			// disconnect
			server_connection_.socket().disconnect();

			// connection is no longer active
			server_connection_.set_active_(false);

			// remove from selector
			selector_.remove(server_connection_.socket());
		}
	}

	// Param in user name to make initial connection with server (also calls send and receive)
	bool ConnectionManagerTCP::ConnectToServer(string& name)
	{
		if (!ConnectToServer()) return false;
		if (!SendToServer(name)) return false;
		string received = ReceiveFromServer();
		if ((received.compare(string("NotReady")) == 0) || (received.compare(string("Disconnected")) == 0) || (received.compare(string("Error")) == 0)) return false;

		return true;
	}

	// Return false if connection fails
	bool ConnectionManagerTCP::ConnectToServer()
	{
		bool result = false;
		
		std::cout << "Connecting to server " << SERVER_IP << "..." << std::endl;

		// Connect to the server
		sf::Socket::Status connect = server_connection_.socket().connect(SERVER_IP, SERVER_PORT, server_connection_.connection_timeout());
		switch (connect)
		{
		case sf::Socket::Done:
			// Success!
			std::cout << "Connected to server " << SERVER_IP << std::endl;
			server_connection_.set_active_(true);
			selector_.add(server_connection_.socket());
			result = true;
			break;
		case sf::Socket::NotReady:
			std::cout << "Failed to connect: Not Ready." << std::endl;
			break;
		case sf::Socket::Disconnected:
			std::cout << "Failed to connect: Disconnected." << std::endl;
			break;
		case sf::Socket::Error:
			std::cout << "Failed to connect: There was an Error." << std::endl;
			int error_code = WSAGetLastError();
			std::cout << "Error Code: " << error_code << "." << std::endl;
			break;
		}
		return result;
	}

	// Attempt to send message to server
	bool ConnectionManagerTCP::SendToServer(string& message)
	{
		std::cout << "Sending message to server: " << message << std::endl;

		return server_connection_.SendMessageTCP(message);
	}

	// Return message received from server
	string ConnectionManagerTCP::ReceiveFromServer()
	{
		std::cout << "Receiving message from server." << std::endl;

		string result = server_connection_.ReceiveMessageTCP();

		std::cout << "Received message from server: " << result << std::endl;

		return result;
	}

	// Returns message received from host
	bool ConnectionManagerTCP::SendToHost(string& message)
	{
		std::cout << "Sending message to Host: " << message << std::endl;

		return host_connection_.SendMessageTCP(message);
	}

	// Attempt to establish a connection with the server
	string ConnectionManagerTCP::ReceiveFromHost()
	{
		std::cout << "Receiving message from host." << std::endl;

		string result = host_connection_.ReceiveMessageTCP();

		std::cout << "Received message from host: " << result << std::endl;

		return result;
	}

	void ConnectionManagerTCP::ConnectToClient(ConnectionTCP* new_client)
	{
		// Inform when client connected
		std::cout << "Client connected: " << new_client->socket().getRemoteAddress() << std::endl;

		// Print message from the new connection
		string response = new_client->ReceiveMessageTCP();
		std::cout << "Response from client: " << response << std::endl;

		// Send a message to the client via the new connection
		string welcome = "Welcome, " + response + ".";
		new_client->SendMessageTCP(welcome);
		
		// this connection is now active
		new_client->set_active_(true);

	}

	bool ConnectionManagerTCP::ConnectToHost(string& message)
	{
		if (!ConnectToHost()) return false;
		if (!SendToHost(message)) return false;
		string received = ReceiveFromHost();
		if ((received.compare(string("NotReady")) == 0) || (received.compare(string("Disconnected")) == 0) || (received.compare(string("Error")) == 0)) return false;

		return true;
	}

	bool ConnectionManagerTCP::ConnectToHost()
	{
		bool result = false;

		std::cout << "Connecting to host "  ; // << "..." << std::endl;

		// Connect to the host
		sf::Socket::Status connect = host_connection_.socket().connect(host_address_, HOST_PORT, server_connection_.connection_timeout());
		switch (connect)
		{
		case sf::Socket::Done:
			// Success!
			std::cout << "Connected to host " << host_address_ << std::endl;
			host_connection_.set_active_(true);
			selector_.add(host_connection_.socket());
			result = true;
			break;
		case sf::Socket::NotReady:
			std::cout << "Failed to connect: Not Ready." << std::endl;
			break;
		case sf::Socket::Disconnected:
			std::cout << "Failed to connect: Disconnected." << std::endl;
			break;
		case sf::Socket::Error:
			std::cout << "Failed to connect: There was an Error." << std::endl;
			int error_code = WSAGetLastError();
			std::cout << "Error Code: " << error_code << "." << std::endl;
			break;
		}
		return result;
	}


}