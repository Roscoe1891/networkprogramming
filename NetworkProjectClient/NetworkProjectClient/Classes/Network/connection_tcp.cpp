#include "connection_tcp.h"

namespace NPC
{
	ConnectionTCP::ConnectionTCP() : connection_timeout_(sf::seconds(2))
	{
		active_ = false;
	}

	ConnectionTCP::~ConnectionTCP()
	{
	}

	bool ConnectionTCP::SendMessageTCP(string& message)
	{
		bool result = false;

		string message_out = BuildMessage(message);

		// Ready buffer to hold outgoing message
		buffer_.clear();
		buffer_.resize(message_out.size());

		// Copy message into buffer
		memcpy(buffer_.data(), message_out.data(), message_out.size());

		// Send message
		sf::Socket::Status send = socket_.send(buffer_.data(), buffer_.size());
		
		switch (send)
		{
		case sf::Socket::Partial:
			std::cout << "Warning - message was only partially sent." << std::endl;
			break;
		case sf::Socket::Done:
			// Success!
			result = true;
			std::cout << "Sent message: " << message << std::endl;
			break;
		case sf::Socket::NotReady:
			std::cout << "Failed to send: Not Ready." << std::endl;
			break;
		case sf::Socket::Disconnected:
			std::cout << "Failed to send: Disconnected." << std::endl;
			break;
		case sf::Socket::Error:
			std::cout << "Failed to send: There was an Error." << std::endl;
			break;
		}
		
		return result;
	}

	string ConnectionTCP::BuildMessage(string& message)
	{
		string result;

		// Get the message size (adding message size)	
		uint16_t message_size = message.size() + sizeof(message_size);

		// Precede with 0 if less than 10
		if (message_size < 10) { result += '0'; }

		// Precede message with message size
		result += std::to_string(message_size);
		result += message;

		return result;
	}

	string ConnectionTCP::ReceiveMessageTCP()
	{
		string message_in;

		// get message size: (read first char of message)
		uint16_t message_size = GetMessageSize();

		// ready buffer for receiving message
		buffer_.clear();
		buffer_.resize(message_size);

		// receive message
		std::size_t received = 0;
		sf::Socket::Status receive = socket_.receive(buffer_.data(), buffer_.size(), received);

		switch (receive)
		{
		case sf::Socket::Done:
			// Success!
			// read from buffer into message in
			message_in = ReadFromBuffer(message_in);
			break;
		case sf::Socket::Partial:
			std::cout << "Uh-oh - message was only partially received." << std::endl;
			break;
		case sf::Socket::NotReady:
			std::cout << "Failed to receive: Not Ready." << std::endl;
			message_in = "NotReady";
			break;
		case sf::Socket::Disconnected:
			std::cout << "Failed to receive: Disconnected." << std::endl;
			message_in = "Disconnected";
			break;
		case sf::Socket::Error:
			std::cout << "Failed to receive: There was an Error." << std::endl;
			message_in = "Error";
			break;
		}
		
		return message_in;
	}

	uint16_t ConnectionTCP::GetMessageSize()
	{
		// get message size: (read first char of message)
		char message_size_buffer[2];

		std::size_t received = 0;
		if (socket_.receive(message_size_buffer, 2, received) != sf::Socket::Done)
		{
			// handle errors
		}

		uint16_t message_size = atoi(message_size_buffer);
		return message_size;
	}

	string ConnectionTCP::ReadFromBuffer(string& message)
	{
		for (int i = 0; i < buffer_.size() - 1; i++)
		{
			if (buffer_[i] != '\0')
				message += buffer_[i];
		}

		return message;
	}

}