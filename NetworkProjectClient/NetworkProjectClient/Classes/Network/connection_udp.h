#ifndef _CONNECTION_UDP_H
#define _CONNECTION_UDP_H

#include <iostream>
#include "SFML\Network.hpp"

#define UDP_TIMEOUT 5

namespace NPC
{
	class ConnectionUDP
	{
	public:

		enum PacketType { TIME_STAMP };

		ConnectionUDP();
		~ConnectionUDP();

		void Init(unsigned short port, sf::IpAddress comm_add, unsigned short comm_port);
		void Update(float dt);
		void CleanUp();

		bool Send(sf::Packet send_packet);
		bool Receive(sf::Packet& receive_packet);

		void SetSocketBlocking(bool new_value);

		sf::Packet CreatePacket(PacketType type, sf::Uint64 time);

		inline sf::UdpSocket& socket() { return socket_; }
		inline const bool& active() { return active_; }
		inline void set_active(bool new_value) { active_ = new_value; }

		inline sf::IpAddress& communication_address() { return communication_address_; }
		inline unsigned short& communication_port() { return communication_port_; }

	private:
		// The UDP socket
		sf::UdpSocket socket_;
		// The address to send to and receive from
		sf::IpAddress communication_address_;
		// The port associated with that address
		unsigned short communication_port_;
		// true if this connection is active
		bool active_;
		// timer to track timeout
		float timeout_timer_;
	};
}

#endif