#ifndef _CONNECTION_TCP_H
#define _CONNECTION_TCP_H

#include <iostream>
#include "SFML\Network.hpp"
#include <string>
#include <vector>

// The maximum message size
#define MAX_MESSAGE_SIZE 97
#define MAX_NAME_SIZE 12

using std::string;
using std::vector;

namespace NPC
{
	class ConnectionTCP
	{
	public:
		ConnectionTCP();
		~ConnectionTCP();

		// return ref to socket
		inline sf::TcpSocket& socket() { return socket_; }
		// return connection timeout
		inline const sf::Time& connection_timeout() { return connection_timeout_; }

		// return value of active
		inline bool active() { return active_; }
		// param[in] new value of active 
		inline void set_active_(bool new_value) { active_ = new_value; }

		// Param[in] string to send as a message across this connection
		bool SendMessageTCP(string& message);
		// Returns string received as a message across this connection
		string ReceiveMessageTCP();

	private:
		// Send utilities
		string BuildMessage(string& message);
		
		// Receive utilities
		uint16_t GetMessageSize();
		string ReadFromBuffer(string& message);

		vector<char> buffer_;
		sf::TcpSocket socket_;
		bool active_;

		const sf::Time connection_timeout_;

	};
}

#endif
