#ifndef _CONNECTION_MANAGER_TCP_H
#define _CONNECTION_MANAGER_TCP_H

#include <WinSock2.h>
#include <list>
#include "connection_tcp.h"

// The server address to connect to
#define SERVER_IP "127.0.0.1"
// The server port to connect to
#define SERVER_PORT 5555
#define HOST_PORT 5556

// Max time to wait for a network operation before abandoning
#define SELECTOR_TIMEOUT 2000
// Number of hosted clients
#define MAX_PLAYERS 2
// Number of hosted clients
#define MAX_HOSTED_CLIENTS 1

namespace NPC
{
	using std::list;

	class ConnectionManagerTCP
	{
	public:
		ConnectionManagerTCP();
		~ConnectionManagerTCP();

		// return value of server connection 'active'
		bool ConnectedToServer();
		// Close connection to server
		void DisconnectFromServer();
		// Param in user name to make initial connection with server (also calls send and receive)
		bool ConnectToServer(string& name);
		// Param[in] message to send to server
		bool SendToServer(string& message);
		// Returns message received from server
		string ReceiveFromServer();

		inline bool ServerIsReady() { return selector_.isReady(server_connection_.socket()); }

		// CLIENT ONLY FUNCTIONS
		bool ConnectToHost(string& message);
		inline ConnectionTCP& host_connection() { return host_connection_; }
		inline const sf::IpAddress& host_address() const { return host_address_; }
		inline void set_host_address(sf::IpAddress& new_address) { host_address_ = new_address; }
		bool SendToHost(string& message);					// Param[in] message to send to host
		string ReceiveFromHost();							// Returns message received from host

		// HOST ONLY FUNCTIONS
		void ConnectToClient(ConnectionTCP* new_client);
		inline sf::TcpListener& host_listener() { return host_listener_; }
		inline sf::SocketSelector& selector() { return selector_; }
		inline const sf::IpAddress* client_addresses() { return client_addresses_; }
		inline void set_client_address_at(sf::IpAddress address, int index) { client_addresses_[index] = address; }
		inline ConnectionTCP* hosted_clients() { return hosted_clients_; }

	private:
		// Attempt to establish a connection with the server
		bool ConnectToServer();
		// Attempt to establish a connection with the host
		bool ConnectToHost();

		// Server connection
		ConnectionTCP server_connection_;
		// Host connection
		ConnectionTCP host_connection_;
		sf::IpAddress host_address_;

		// HOST ONLY: Host listener
		sf::TcpListener host_listener_;
		// HOST ONLY: array for hosted clients
		NPC::ConnectionTCP hosted_clients_[MAX_HOSTED_CLIENTS];
		// HOST ONLY: array for hosted client addresses
		sf::IpAddress client_addresses_[MAX_HOSTED_CLIENTS];

		// Socket Selector
		sf::SocketSelector selector_;

	};
}

#endif // !_CONNECTION_MANAGER_H
