#include "state_exit.h"

namespace NPC
{
	StateExit::StateExit()
	{
		state_type_ = EXIT;
		next_state_type_ = state_type_;
	}

	StateExit::~StateExit()
	{
	}

	// Intialise State
	void StateExit::StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer)
	{
		std::cout << "Initialising the Exit State." << std::endl;

		next_state_type_ = state_type_;

		connection_manager_tcp_ = connection_manager;

		user_data_ = user_data;
	}

	// Update the State (param[in] delta time)
	void StateExit::StateUpdate(const float& dt, sf::RenderWindow* window)
	{
		// Disconnect from server
		connection_manager_tcp_->DisconnectFromServer();

		// close window
		window->close();
	}

	// Draw State (param[in] pointer to window)
	void StateExit::StateRender(sf::RenderWindow* window)
	{

	}

	// Clean Up
	void StateExit::StateCleanUp()
	{

	}


}