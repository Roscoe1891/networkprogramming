#include "state_connect_host.h"

namespace NPC
{
	StateConnectHost::StateConnectHost() : connect_thread_(nullptr)
	{
		state_type_ = CONNECT_HOST;
		next_state_type_ = state_type_;
	}

	StateConnectHost::~StateConnectHost()
	{
	}

	// Intialise State
	void StateConnectHost::StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer)
	{
		std::cout << "Initialising the Connect State as a Host." << std::endl;

		connection_manager_tcp_ = connection_manager;

		next_state_type_ = state_type_;

		user_data_ = user_data;

		listening_mutex_.lock();
		listening_ = true;
		listening_mutex_.unlock();

		// bind listener to server port
		if (connection_manager_tcp_->host_listener().listen(HOST_PORT) != sf::Socket::Done)
		{
			std::cout << "Host listen call was unsuccessful." << std::endl;
		}
		std::cout << "Host is listening to port " << HOST_PORT << ", waiting for connections... " << std::endl;

		// add listener to selector
		connection_manager_tcp_->selector().add(connection_manager_tcp_->host_listener());
		
		connect_thread_ = new thread(&StateConnectHost::Connect, this);

		// set up state text
		state_text_.resize(CONNECT_HOST_STATE_TEXT);

		for (int i = 0; i < state_text_.size(); ++i)
		{
			state_text_[i].setFont(*font);
			state_text_[i].setCharacterSize(24);
			state_text_[i].setPosition(0, i * 50);
		}
		state_text_[0].setString("In the Connect State as a Host.");
	}
	
	// Update the State (param[in] delta time)
	void StateConnectHost::StateUpdate(const float& dt, sf::RenderWindow* window)
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				if (next_state_type_ != EXIT) next_state_type_ = EXIT;
				break;
			case sf::Event::TextEntered:
				// exit window on pressing escape
				if (event.text.unicode == 27)
				{
					if (next_state_type_ != EXIT) next_state_type_ = EXIT;
				}
				break;
			default:
				break;
			}
		}

		if (all_clients_connected_)
		{
			std::cout << "Changing to play state as host" << std::endl;
			if (next_state_type_ != PLAY_HOST) next_state_type_ = PLAY_HOST;
			listening_mutex_.lock();
			listening_ = false;
			listening_mutex_.unlock();
		}
	}
	
	// Draw State (param[in] pointer to window)
	void StateConnectHost::StateRender(sf::RenderWindow* window)
	{
		for (int i = 0; i < state_text_.size(); ++i)
		{
			window->draw(state_text_[i]);
		}
	}

	// Clean Up
	void StateConnectHost::StateCleanUp()
	{
		if ((connect_thread_->joinable()) && (connect_thread_ != nullptr))
		{
			connect_thread_->join();
			delete connect_thread_;
			connect_thread_ = nullptr;
		}
	}

	// return true if all clients are active
	bool StateConnectHost::AllClientsConnected()
	{
		bool result = false;
		for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
		{
			if (!connection_manager_tcp_->hosted_clients()[i].active())
				return result;
		}
		result = true;
		return result;
	}

	// State thread function for client
	void StateConnectHost::Connect()
	{
		while (listening_)
		{
			// Make selector wait on data from any socket
			if (connection_manager_tcp_->selector().wait(sf::milliseconds(SELECTOR_TIMEOUT)))
			{
				// if listener is ready
				if (connection_manager_tcp_->selector().isReady(connection_manager_tcp_->host_listener()))
				{
					ConnectionTCP* new_connection = nullptr;

					// find first free socket
					for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
					{
						if (!connection_manager_tcp_->hosted_clients()[i].active())
						{
							new_connection = &connection_manager_tcp_->hosted_clients()[i];
							break;
						}
					}

					// If connection was successful
					if (connection_manager_tcp_->host_listener().accept(new_connection->socket()) == sf::Socket::Done)
					{
						// Add new connection
						connection_manager_mutex_.lock();
						connection_manager_tcp_->ConnectToClient(new_connection);
						connection_manager_mutex_.unlock();

						// add new connection to selector
						connection_manager_tcp_->selector().add(new_connection->socket());

						// check if all clients are connected
						all_clients_connected_mutex_.lock();
						all_clients_connected_ = AllClientsConnected();
						all_clients_connected_mutex_.unlock();
					}
					// if unsuccessful
					else
					{
						std::cout << "There was a problem accepting the new connection." << std::endl;
						delete new_connection;
						new_connection = nullptr;
					}
				}
				// otherwise check messages from clients
				else
				{

				}
			}
		}
	}

}