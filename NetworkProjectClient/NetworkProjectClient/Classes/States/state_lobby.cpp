#include "state_lobby.h"

namespace NPC
{
	StateLobby::StateLobby() : waiting_thread_(nullptr)
	{
		state_type_ = LOBBY;
		next_state_type_ = state_type_;
	}

	StateLobby::~StateLobby()
	{
	}

	// Intialise State
	void StateLobby::StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer)
	{
		std::cout << "Initialising the Lobby State." << std::endl;

		next_state_type_ = state_type_;

		connection_manager_tcp_ = connection_manager;

		user_data_ = user_data;

		waiting_ = true;

		found_game_ = false;

		state_text_.resize(LOBBY_STATE_TEXT);

		for (int i = 0; i < state_text_.size(); ++i)
		{
			state_text_[i].setFont(*font);
			state_text_[i].setCharacterSize(24);
			state_text_[i].setPosition(0, i * 50);
		}
		state_text_[0].setString("Logged in to the Server as " + user_data_->user_name());
		state_text_[1].setString("Waiting for opponents.");
		state_text_[2].setString("Press escape to quit.");

		waiting_thread_ = new thread(&StateLobby::WaitForGame, this);

	}

	// Update the State (param[in] delta time)
	void StateLobby::StateUpdate(const float& dt, sf::RenderWindow* window)
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				StopWaiting();
				if (next_state_type_ != EXIT) next_state_type_ = EXIT;
				break;
			case sf::Event::TextEntered:
				// exit window on pressing escape
				if (event.text.unicode == 27)
				{
					StopWaiting();
					if (next_state_type_ != EXIT) next_state_type_ = EXIT;
				}
				break;
			default:
				break;
			}
		}

		if (found_game_)
		{
			StopWaiting();
			if (user_data_->host())
			{
				if (next_state_type_ != CONNECT_HOST) next_state_type_ = CONNECT_HOST;
			}
			else
			{
				if (next_state_type_ != CONNECT_CLIENT) next_state_type_ = CONNECT_CLIENT;

			}
		}

	}

	// Draw State (param[in] pointer to window)
	void StateLobby::StateRender(sf::RenderWindow* window)
	{
		for (int i = 0; i < state_text_.size(); ++i)
		{
			window->draw(state_text_[i]);
		}
	}

	// Clean Up
	void StateLobby::StateCleanUp()
	{
		delete waiting_thread_;
		waiting_thread_ = nullptr;
	}

	// Wait for Notification from server that a game has been set up
	void StateLobby::WaitForGame()
	{
		string received;
		bool received_new_message = false;

		// only wait while there is no game ready
		while (waiting_)
		{
			received_new_message = false;

			// receive message from server
			connection_manager_mutex_.lock();
			if (connection_manager_tcp_->selector().wait(sf::milliseconds(SELECTOR_TIMEOUT)))
			{
				if (connection_manager_tcp_->ServerIsReady())
				{
					received = connection_manager_tcp_->ReceiveFromServer();
					received_new_message = true;
				}
			}
			connection_manager_mutex_.unlock();

			// If a game has not yet been found
			if ((!found_game_) && (received_new_message))
			{
				// check received message
				if (received.compare(1, 8, string("Host IP:")) == 0)
				{
					SetUpAsClient(received);
				}
				else if (received.compare(1, 17, string("You are the host.")) == 0)
				{
					SetUpAsHost(received);
				}
				else if (received.compare(string("Good-bye from the server")) == 0)
				{
					std::cout << "Server has disconnected this client." << std::endl;

					// disconnect
					connection_manager_mutex_.lock();
					connection_manager_tcp_->DisconnectFromServer();
					connection_manager_mutex_.unlock();

					// stop waiting
					waiting_mutex_.lock();
					waiting_ = false;
					waiting_mutex_.unlock();

					// update UI
					state_text_[0].setString("The server has disconnected you.");
					state_text_[1].setString("");
					state_text_[2].setString("Press escape to quit.");
				}
				else if (received.compare(string("Disconnected")) == 0)
				{
					std::cout << "Disconnected from client." << std::endl;

					// disconnect
					connection_manager_mutex_.lock();
					connection_manager_tcp_->DisconnectFromServer();
					connection_manager_mutex_.unlock();

					// stop waiting
					waiting_mutex_.lock();
					waiting_ = false;
					waiting_mutex_.unlock();

					// update UI
					state_text_[0].setString("You are no longer connected to the server.");
					state_text_[1].setString("");
					state_text_[2].setString("Press escape to quit.");
				}
			}
			
		}
	}

	// Stop waiting for data from server
	// Attempts to join waiting thread 
	void StateLobby::StopWaiting()
	{
		// stop waiting
		waiting_mutex_.lock();
		waiting_ = false;
		waiting_mutex_.unlock();
		// joing waiting thread
		if ((waiting_thread_ != nullptr) && (waiting_thread_->joinable())) waiting_thread_->join();
	}

	void StateLobby::SetUpAsClient(string& client_data)
	{
		std::cout << "I will be a client in this game." << std::endl;
		user_data_->set_host(false);

		sf::IpAddress host_add(string(client_data.begin() + 9, client_data.end()));
		connection_manager_mutex_.lock();
		connection_manager_tcp_->set_host_address(host_add);
		connection_manager_mutex_.unlock();

		user_data_mutex_.lock();
		if (string(client_data, 0, 1) == "0") user_data_->set_player_number((sf::Int8)0);
		else if (string(client_data, 0, 1) == "1") user_data_->set_player_number((sf::Int8)1);
		else if (string(client_data, 0, 1) == "2") user_data_->set_player_number((sf::Int8)2);
		else if (string(client_data, 0, 1) == "3") user_data_->set_player_number((sf::Int8)3);
		user_data_mutex_.unlock();

		std::cout << "My player number is " << static_cast<int>(user_data_->player_number()) << std::endl;

		//host_add = user_data_->host_address();
		//string host_add_str_ = host_add.toString();
		//std::cout << "I've recorded the Host IP as: " << host_add_str_ << std::endl;

		// found game
		found_game_mutex_.lock();
		found_game_ = true;
		found_game_mutex_.unlock();
	}

	void StateLobby::SetUpAsHost(string& host_data)
	{
		std::cout << "I will be the host for this game." << std::endl;

		user_data_->set_host(true);

		string player_num_str = string(host_data, 0, 1);
		int player_num = std::stoi(player_num_str);
		user_data_mutex_.lock();
		user_data_->set_player_number(player_num);
		user_data_mutex_.unlock();

		// populate client addresses from this
		string client_addrs = string(host_data, 18);
		sf::IpAddress addr = sf::IpAddress::None;
		string addr_str = "";
		int addr_index = 0;

		for (auto it = client_addrs.begin(); it != client_addrs.end(); ++it)
		{
			// if it isn't delimiter
			if ((*it) != ',')
			{
				// add char to address
				addr_str += (*it);
			}
			// otherwise
			else
			{
				// make address
				addr = sf::IpAddress(addr_str);
				// store address
				connection_manager_mutex_.lock();
				connection_manager_tcp_->set_client_address_at(addr, addr_index);
				connection_manager_mutex_.unlock();
				// clear addr str
				addr_str = "";
				// increment addr_index
				addr_index++;
			}
		}

		// Debug: Print out client addresses
		for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
		{
			std::cout << "Client address at " << i << ": " << connection_manager_tcp_->client_addresses()[i] << std::endl;
		}

		std::cout << "My player number is " << user_data_->player_number() << std::endl;

		// found game
		found_game_mutex_.lock();
		found_game_ = true;
		found_game_mutex_.unlock();

	}


}