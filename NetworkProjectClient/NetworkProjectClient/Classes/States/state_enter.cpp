#include "state_enter.h"

namespace NPC
{
	StateEnter::StateEnter() : connection_timeout_(sf::seconds(10)), connection_thread_(nullptr)
	{
		state_type_ = ENTER;
		connecting_ = false;
		connected_ = false;
		next_state_type_ = state_type_;
	}

	StateEnter::~StateEnter()
	{

	}

	// Intialise State
	void StateEnter::StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer)
	{
		std::cout << "Initialising the Enter State." << std::endl;

		connection_manager_tcp_ = connection_manager;

		connecting_ = false;
		connected_ = false;

		next_state_type_ = state_type_;

		user_data_ = user_data;
		user_data_->set_user_name(string(""));

		state_text_.resize(ENTER_STATE_TEXT);

		for (int i = 0; i < state_text_.size(); ++i)
		{
			state_text_[i].setFont(*font);
			state_text_[i].setCharacterSize(24);
			state_text_[i].setPosition(0, i * 50);
		}
		state_text_[0].setString("Please enter your name below (max 12 chars).");
		state_text_[1].setString("Press return to connect to server.");
		state_text_[2].setString("Press escape to quit.");
		state_text_[3].setString("");
		// state text 4 used to show connection status
		state_text_[4].setString("");
		
	}
	
	// Update the State (param[in] delta time)
	void StateEnter::StateUpdate(const float& dt, sf::RenderWindow* window)
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				if ((connection_thread_ != nullptr) && (connection_thread_->joinable())) connection_thread_->join();
				if (next_state_type_ != EXIT) next_state_type_ = EXIT;
				break;

			case sf::Event::TextEntered:
				if (!connecting_) EnterText(&event);
				break;
			default:
				break;
			}
		}

		// change to lobby state on successful connection to server
		if (connected_)
		{
			if (connection_thread_->joinable()) connection_thread_->join();
			{
				user_data_->set_user_name(name_string_);
				next_state_type_ = LOBBY;
			}
		}
	}
	
	// Draw State (param[in] pointer to window)
	void StateEnter::StateRender(sf::RenderWindow* window)
	{
		for (int i = 0; i < state_text_.size(); ++i)
		{
			window->draw(state_text_[i]);
		}
	}
	
	// Clean Up
	void StateEnter::StateCleanUp()
	{
		name_string_ = "";

		delete connection_thread_;
		connection_thread_ = nullptr;
	}

	void StateEnter::EnterText(sf::Event* text_event)
	{
		// Check for valid characters
		if ((text_event->text.unicode >= 32) && (text_event->text.unicode <= 126))
		{
			name_string_ += static_cast<char>(text_event->text.unicode);
			state_text_[3].setString(name_string_);
		}
		// Check for deleted characters
		else if ((text_event->text.unicode == 8))
		{
			name_string_.erase(name_string_.size() - 1);
			state_text_[3].setString(name_string_);
		}
		// If return pressed, try to connect (if name entered)
		else if (text_event->text.unicode == 13)
		{
			if ((name_string_.size() > 0) && (name_string_.size() <= MAX_NAME_SIZE))
			{
				connection_thread_ = new thread(&StateEnter::ConnectToServer, this);
			}
		}
		// exit window on pressing escape
		else if (text_event->text.unicode == 27)
		{
			if ((connection_thread_ != nullptr) && (connection_thread_->joinable())) connection_thread_->join();
			if (next_state_type_ != EXIT) next_state_type_ = EXIT;
		}
	}

	void StateEnter::ConnectToServer()
	{
		// connect
		state_text_mutex_.lock();
		state_text_[4].setString("Connecting...");
		state_text_mutex_.unlock();

		connecting_mutex_.lock();
		connecting_ = true;
		connecting_mutex_.unlock();

		connection_manager_mutex_.lock();
		if (connection_manager_tcp_->ConnectToServer(name_string_))
		{
			connected_mutex_.lock();
			connected_ = true;
			connected_mutex_.unlock();

			state_text_mutex_.lock();
			state_text_[4].setString("Success!");
			state_text_mutex_.unlock();
		}
		else
		{
			std::cout << "Unable to connect. Press Return to try again." << std::endl;

			state_text_mutex_.lock();
			state_text_[3].setString("");
			state_text_[4].setString("Connection attempt failed.");
			state_text_mutex_.unlock();

			name_string_mutex_.lock();
			name_string_ = "";
			name_string_mutex_.unlock();


		}
		connection_manager_mutex_.unlock();

		connecting_mutex_.lock();
		connecting_ = false;
		connecting_mutex_.unlock();

	}


}