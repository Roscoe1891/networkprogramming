#ifndef _STATE_H
#define _STATE_H

#include <iostream>
#include <vector>
#include <string>
#include <thread>
#include <mutex>
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include "Network\connection_manager_tcp.h"
#include "User\user_data.h"
#include "Utilities\timer.h"

// The base class for game states
using std::mutex;
using std::string;
using std::thread;

namespace NPC
{
	class State
	{

	public:
		enum StateType { ENTER, LOBBY, CONNECT_HOST, CONNECT_CLIENT, PLAY_HOST, PLAY_CLIENT, EXIT };

		State();
		virtual ~State();

		// Intialise State
		virtual void StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer = nullptr) = 0;
		// Update the State (param[in] delta time)
		virtual void StateUpdate(const float& dt, sf::RenderWindow* window) = 0;
		// Draw State (param[in] pointer to window)
		virtual void StateRender(sf::RenderWindow* window) = 0;
		// Clean Up
		virtual void StateCleanUp() = 0;

		inline const StateType state_type() { return state_type_; }
		inline const StateType next_state_type() { return next_state_type_; }

	protected:
		StateType next_state_type_;
		StateType state_type_;
		std::vector<sf::Text> state_text_;
		ConnectionManagerTCP* connection_manager_tcp_;
		UserData* user_data_;		
		mutex connection_manager_mutex_;


		
	};
}

#endif