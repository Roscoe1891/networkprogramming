#ifndef _STATE_PLAY_HOST_H
#define _STATE_PLAY_HOST_H

#include "state.h"
#include "GameObjects\player.h"
#include "GameObjectViews\game_object_view.h"
#include "GameObjectCtrls\player_controller.h"
#include "GameObjectCtrls\predictive_controller.h"
#include "Map\map_view.h"
#include "Network\connection_udp.h"
#include "Physics\physics_2d.h"

#define PLAY_STATE_TEXT_HOST 3
//#define MAX_PLAYERS 3
//#define LAG_UPDATE 1000000

namespace NPC
{
	using std::vector;

	class StatePlayHost : public State
	{
	public:
		StatePlayHost();
		~StatePlayHost();
		// Intialise State
		virtual void StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer = nullptr);
		// Update the State (param[in] delta time)
		virtual void StateUpdate(const float& dt, sf::RenderWindow* window);
		// Draw State (param[in] pointer to window)
		virtual void StateRender(sf::RenderWindow* window);
		// Clean Up
		virtual void StateCleanUp();
		
	private:
		void InitPlayers();
		void StartClients();
		bool AllClientsActive();
		void SendStateUpdates();
		void ReadStateUpdate(sf::Packet& packet, Timer* timer);

		Map map_;
		MapView map_view_;
		
		Player players_[MAX_PLAYERS];
		GameObjectView player_views_[MAX_PLAYERS];
		PlayerController user_player_controller_;
		PredictiveController predictive_controllers_[MAX_HOSTED_CLIENTS];

		ConnectionUDP hosted_clients_udp_[MAX_PLAYERS];

		Timer* timer_;
		float state_update_timer_, state_update_time_;
		sf::Int64 last_lagtime_update_[MAX_HOSTED_CLIENTS];

		Physics2D physics_;

	};

}

#endif // !_STATE_PLAY_H