#ifndef _STATE_LOBBY_H
#define _STATE_LOBBY_H

#include "state.h"

// The Lobby State is where the client waits for the server to set up a four player game

#define LOBBY_STATE_TEXT 3

namespace NPC
{
	class StateLobby : public State
	{
	public:

		StateLobby();
		~StateLobby();
		// Intialise State
		virtual void StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer = nullptr);
		// Update the State (param[in] delta time)
		virtual void StateUpdate(const float& dt, sf::RenderWindow* window);
		// Draw State (param[in] pointer to window)
		virtual void StateRender(sf::RenderWindow* window);
		// Clean Up
		virtual void StateCleanUp();

	private:
		// Try to receive game data from server
		void WaitForGame();
		// Stop waiting for data from server
		void StopWaiting();

		// Set this user up as a client
		void SetUpAsClient(string& client_data);
		// Set this user up as a host
		void SetUpAsHost(string& host_data);

		thread* waiting_thread_;

		bool waiting_;
		mutex waiting_mutex_;

		bool found_game_;
		mutex found_game_mutex_;
		mutex user_data_mutex_;
	};

}

#endif // !_STATE_LOBBY_H