#include "state_play_client.h"

namespace NPC
{
	StatePlayClient::StatePlayClient()
	{
		state_type_ = PLAY_CLIENT;
		next_state_type_ = state_type_;
		lag_time_ = 0;
		state_update_time_ = 0.04f;
		state_update_timer_ = 0;
	}

	StatePlayClient::~StatePlayClient()
	{

	}

	// Intialise State
	void StatePlayClient::StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer)
	{
		std::cout << "Initialising the Play State." << std::endl;

		next_state_type_ = state_type_;

		connection_manager_tcp_ = connection_manager;
		
		user_data_ = user_data;

		timer_ = timer;

		// Init Map
		map_.Init();
		map_view_.Init(&map_);

		// Init Physics
		physics_.Init(&map_);

		// Init Players
		InitPlayers();

		// Init Text
		state_text_.resize(PLAY_STATE_TEXT_CLIENT);

		for (int i = 0; i < state_text_.size(); ++i)
		{
			state_text_[i].setFont(*font);
			state_text_[i].setCharacterSize(24);
			state_text_[i].setPosition(0, i * 50);
		}

		state_text_[0].setString("In Play State as: " + user_data_->user_name());
		state_text_[1].setString("Press escape to quit.");

		// Block until start command received from host
		string received_from_host = connection_manager_tcp_->ReceiveFromHost();
		if (received_from_host.compare(0, 5, "Start") == 0)
		{
			// get remote port
			unsigned short remote_port = std::stoi(received_from_host.substr(5));

			std::cout << "remote port is " << remote_port << std::endl;

			// init udp connection
			host_connection_.Init(connection_manager_tcp_->host_connection().socket().getLocalPort(), connection_manager_tcp_->host_address(), remote_port);
			// set to non block
			host_connection_.SetSocketBlocking(false);

			// reset clock
			std::cout << "restarting game clock" << std::endl;
			timer->ResetGameTime();

			// packet to store return message
			sf::Packet response;
			while (!host_connection_.Receive(response))
			{
				// std::cout << "sending a time stamp message to the host" << std::endl;
				// create packet with time stamp
				sf::Uint64 time = timer->GameTime();
				sf::Packet time_stamp = host_connection_.CreatePacket(ConnectionUDP::TIME_STAMP, time);
				// send to host
				host_connection_.Send(time_stamp);
			}
			// update our lag time
			UpdateLagTime(response);
		}

		// set host connection to non-blocking
		host_connection_.SetSocketBlocking(false);

	}

	void StatePlayClient::InitPlayers()
	{
		// Set up player data
		float player_size = 20.f;
		//float offset = (TILE_SIZE - player_size) * 0.5;
		float mid = 320;
		float low = 120;
		float high = 520;


		vector<sf::Vector2f> start_positions;
		vector<sf::Color> player_colours;

		switch (MAX_PLAYERS)
		{
		case 2:
			start_positions.push_back(sf::Vector2f(mid, high));
			start_positions.push_back(sf::Vector2f(mid, low));

			player_colours.push_back(sf::Color(0, 255, 255));
			player_colours.push_back(sf::Color(255, 0, 255));

			break;
		case 3:
			start_positions.push_back(sf::Vector2f(high, mid));
			start_positions.push_back(sf::Vector2f(low, mid));
			start_positions.push_back(sf::Vector2f(mid, high));

			player_colours.push_back(sf::Color(0, 255, 255));
			player_colours.push_back(sf::Color(255, 0, 255));
			player_colours.push_back(sf::Color(255, 255, 255));

			break;
		case 4:
			start_positions.push_back(sf::Vector2f(high, mid));
			start_positions.push_back(sf::Vector2f(low, mid));
			start_positions.push_back(sf::Vector2f(mid, low));
			start_positions.push_back(sf::Vector2f(mid, high));

			player_colours.push_back(sf::Color(0, 255, 255));
			player_colours.push_back(sf::Color(255, 0, 255));
			player_colours.push_back(sf::Color(0, 255, 0));
			player_colours.push_back(sf::Color(255, 255, 255));

			break;
		default:
			break;

		}

		// Init player models, views & controllers
		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			players_[i].Init(sf::Vector2f(player_size, player_size), start_positions[i]);
			player_views_[i].Init(&players_[i], player_colours[i]);

			if (i < user_data_->player_number())
			{
				predictive_controllers_[i].Init(&players_[i]);
			}
			else if (i > user_data_->player_number())
			{
				predictive_controllers_[i - 1].Init(&players_[i]);
			}
			else
			{
				user_player_controller_.Init(&players_[user_data_->player_number()]);
			}
		}
	}

	// Update the State (param[in] delta time)
	void StatePlayClient::StateUpdate(const float& dt, sf::RenderWindow* window)
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window->close();
				break;
			case sf::Event::TextEntered:
				// exit window on pressing escape
				if (event.text.unicode == 27)
				{
					if (next_state_type_ != EXIT) next_state_type_ = EXIT;
				}
				break;
			case sf::Event::KeyPressed:
				user_player_controller_.ReceivedKeyInput(event.key, true);
				break;
			case sf::Event::KeyReleased:
				user_player_controller_.ReceivedKeyInput(event.key, false);
				break;
			default:
				break;
			}
		}

		// update own player
		user_player_controller_.Update(dt);
		
		// check own physics
		physics_.Collision_BB_Tilemap(players_[user_data_->player_number()].bounding_box());

		// check state update timer
		if (state_update_timer_ < state_update_time_)
		{
			state_update_timer_ += dt;
		}
		else
		{
			// send update to host
			SendStateUpdate();

			// zero timer
			state_update_timer_ = 0;
		}

		// check for updates from host...
		sf::Packet host_update;
		if (host_connection_.Receive(host_update))
		{
			//std::cout << "Received an update from the host" << std::endl;
			ReadStateUpdate(host_update, timer_);
		}
		host_connection_.Update(dt);
		
		if (!host_connection_.active())
		{
			if (state_text_[2].getString() == "")
			{
				state_text_[2].setString("Warning: Connection interrupted.");
			}
		}

		// update predictive controllers
		for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
		{
			predictive_controllers_[i].Update(timer_);
		}

		//for (int i = 0; i < MAX_PLAYERS; ++i)
		//{
		//	std::cout << "player " << i << "pos " << "is" << players_[i].bounding_box().position().x << ", " << players_[i].bounding_box().position().y << std::endl;
		//}

		// check collision vs map
		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			if (i != user_data_->player_number())
				physics_.Collision_BB_Tilemap(players_[user_data_->player_number()].bounding_box());
		}
		
		// update all views
		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			player_views_[i].Update();
		}
	}

	void StatePlayClient::SendStateUpdate()
	{
		sf::Packet state_update;
		// state update made of: Time stamp, player ID, player x pos, player y pos
		state_update << timer_->GameTime() << user_data_->player_number() << 
			players_[user_data_->player_number()].bounding_box().position().x << players_[user_data_->player_number()].bounding_box().position().y;
		// send update to host
		host_connection_.Send(state_update);
	}

	// Read a state update and pass it on to the relevant controller
	void StatePlayClient::ReadStateUpdate(sf::Packet& packet, Timer* timer)
	{
		// containers for data
		sf::Int64 time_stamp = 0;
		sf::Int8 player_ids[MAX_PLAYERS] = {0};
		float pos_x[MAX_PLAYERS] = {0.f};
		float pos_y[MAX_PLAYERS] = {0.f};
		bool read = false;


		// read packet based on number of players
		switch (MAX_PLAYERS)
		{
			case 2:
				if (packet >> time_stamp >>
					player_ids[0] >> pos_x[0] >> pos_y[0] >>
					player_ids[1] >> pos_x[1] >> pos_y[1])
					read = true;
				break;
			case 3:
				if (packet >> time_stamp >>
					player_ids[0] >> pos_x[0] >> pos_y[0] >>
					player_ids[1] >> pos_x[1] >> pos_y[1] >>
					player_ids[2] >> pos_x[2] >> pos_y[2])
					read = true;
				break;
			case 4:
				if (packet >> time_stamp >>
					player_ids[0] >> pos_x[0] >> pos_y[0] >>
					player_ids[1] >> pos_x[1] >> pos_y[1] >>
					player_ids[2] >> pos_x[2] >> pos_y[2] >>
					player_ids[3] >> pos_x[3] >> pos_y[3])
					read = true;
				break;
		}

		if (read)
		{
			// data read successfully
			for (int i = 0; i < MAX_PLAYERS; ++i)
			{
				//std::cout << "Received: " << time_stamp << ", " << static_cast<int>(player_ids[i]) << ", " << pos_x[i] << ", " << pos_y[i] << std::endl;

				// if this isn't 'me'
				if (i == user_data_->player_number())
					continue;
				else
				{
					// create a position update
					PositionUpdate pos_update;
					pos_update.sent_time = time_stamp;
					pos_update.received_time = timer->GameTime();
					pos_update.position = sf::Vector2f(pos_x[i], pos_y[i]);
					// adjust index (since there are only 3 predictive controllers we have data for 4 players)
					// if we are looking at a player with lower player number than ours, no problem
					if (i < user_data_->player_number())
					{
						predictive_controllers_[i].ReceivePositionUpdate(pos_update);
					}
					// otherwise subtract one from index (so 'skip' us)
					else
					{
						predictive_controllers_[i-1].ReceivePositionUpdate(pos_update);
					}
				}				
			}
		}
		else if (packet >> time_stamp)
		{
			UpdateLagTime(time_stamp);
		}
		else
		{
			std::cout << "There was a problem reading the packet." << std::endl;
		}

	}

	// Draw State (param[in] pointer to window)
	void StatePlayClient::StateRender(sf::RenderWindow* window)
	{

		map_view_.Render(window);

		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			player_views_[i].Render(window);
		}

		for (int i = 0; i < state_text_.size(); ++i)
		{
			window->draw(state_text_[i]);
		}
	}

	void StatePlayClient::UpdateLagTime(sf::Packet packet)
	{
		// read from packet to get time of sending
		sf::Uint64 packet_send_time;
		packet >> packet_send_time;

		UpdateLagTime(packet_send_time);
	}

	void StatePlayClient::UpdateLagTime(sf::Int64 time_stamp)
	{
		// subtract from actual time to get round trip time
		lag_time_ = timer_->GameTime() - time_stamp;
		// half to get lag time;
		lag_time_ *= 0.5;

		std::cout << "Lag time is: " << lag_time_ << std::endl;
	}

	// Clean Up
	void StatePlayClient::StateCleanUp()
	{

	}

}