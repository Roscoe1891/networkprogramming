#ifndef _STATE_CONNECT_HOST_H
#define _STATE_CONNECT_HOST_H

#include "state.h"
#include <thread>
#include <mutex>

#define CONNECT_HOST_STATE_TEXT 1

namespace NPC
{
	using std::thread;
	using std::mutex;

	class StateConnectHost : public State
	{
	public:
		StateConnectHost();
		~StateConnectHost();

		// Intialise State
		virtual void StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer = nullptr);
		// Update the State (param[in] delta time)
		virtual void StateUpdate(const float& dt, sf::RenderWindow* window);
		// Draw State (param[in] pointer to window)
		virtual void StateRender(sf::RenderWindow* window);
		// Clean Up
		virtual void StateCleanUp();

	private:
		// connect to a client
		void Connect();
		// return true if all three client connecitons are active
		bool AllClientsConnected();

		// State thread function for host
		thread* connect_thread_;
		bool listening_;
		mutex listening_mutex_;
		bool all_clients_connected_;
		mutex all_clients_connected_mutex_;
	

	};
}

#endif // !_STATE_CONNECT_H
