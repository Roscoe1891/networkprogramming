#include "state_connect_client.h"

namespace NPC
{
	StateConnectClient::StateConnectClient() : connect_thread_(nullptr)
	{
		state_type_ = CONNECT_CLIENT;
		next_state_type_ = state_type_;
	}

	StateConnectClient::~StateConnectClient()
	{
	}

	// Intialise State
	void StateConnectClient::StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer)
	{
		std::cout << "Initialising the Connect State as a Client." << std::endl;

		connection_manager_tcp_ = connection_manager;

		next_state_type_ = state_type_;

		user_data_ = user_data;

		// set up state text
		state_text_.resize(CONNECT_CLIENT_STATE_TEXT);

		for (int i = 0; i < state_text_.size(); ++i)
		{
			state_text_[i].setFont(*font);
			state_text_[i].setCharacterSize(24);
			state_text_[i].setPosition(0, i * 50);
		}
		state_text_[0].setString("In the Connect State as a Client.");

		connect_thread_ = new thread(&StateConnectClient::Connect, this);

	}
	
	// Update the State (param[in] delta time)
	void StateConnectClient::StateUpdate(const float& dt, sf::RenderWindow* window)
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				if (next_state_type_ != EXIT) next_state_type_ = EXIT;
				break;
			case sf::Event::TextEntered:
				// exit window on pressing escape
				if (event.text.unicode == 27)
				{
					if (next_state_type_ != EXIT) next_state_type_ = EXIT;
				}
				break;
			default:
				break;
			}
		}

		if ((!connecting_) && (connected_))
		{
			std::cout << "Changing to play state as client" << std::endl;

			// disconnect from server
			connection_manager_tcp_->DisconnectFromServer();

			// set flag to switch to play state
			if (next_state_type_ != PLAY_CLIENT) next_state_type_ = PLAY_CLIENT;
		}
	}
	
	// Draw State (param[in] pointer to window)
	void StateConnectClient::StateRender(sf::RenderWindow* window)
	{
		for (int i = 0; i < state_text_.size(); ++i)
		{
			window->draw(state_text_[i]);
		}
	}

	// Clean Up
	void StateConnectClient::StateCleanUp()
	{
		if ((connect_thread_->joinable()) && (connect_thread_ != nullptr))
		{
			connect_thread_->join();
			delete connect_thread_;
			connect_thread_ = nullptr;
		}
	}

	// State thread function for client
	void StateConnectClient::Connect()
	{
		// connect
		std::cout << "Connecting to Host..." << std::endl;

		connecting_mutex_.lock();
		connecting_ = true;
		connecting_mutex_.unlock();

		connection_manager_mutex_.lock();
		if (connection_manager_tcp_->ConnectToHost(string(user_data_->user_name() + " here!")))
		{
			connected_mutex_.lock();
			connected_ = true;
			connected_mutex_.unlock();
		}
		else
		{
			std::cout << "Unable to connect to host." << std::endl;
		}
		connection_manager_mutex_.unlock();

		connecting_mutex_.lock();
		connecting_ = false;
		connecting_mutex_.unlock();
	}
}