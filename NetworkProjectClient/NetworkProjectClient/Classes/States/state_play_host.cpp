#include "state_play_host.h"

namespace NPC
{
	StatePlayHost::StatePlayHost() : timer_(nullptr)
	{
		state_type_ = PLAY_HOST;
		next_state_type_ = state_type_;
		state_update_time_ = 0.04f;
		state_update_timer_ = 0;
	}

	StatePlayHost::~StatePlayHost()
	{
	}

	// Intialise State
	void StatePlayHost::StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer)
	{
		std::cout << "Initialising the Play State as a Host." << std::endl;

		next_state_type_ = state_type_;

		connection_manager_tcp_ = connection_manager;
		
		user_data_ = user_data;

		timer_ = timer;

		// Init Map
		map_.Init();
		map_view_.Init(&map_);

		// Init Physics
		physics_.Init(&map_);

		// Init Players
		InitPlayers();

		// Init Text
		state_text_.resize(PLAY_STATE_TEXT_HOST);

		for (int i = 0; i < state_text_.size(); ++i)
		{
			state_text_[i].setFont(*font);
			state_text_[i].setCharacterSize(24);
			state_text_[i].setPosition(0, i * 50);
		}

		state_text_[0].setString("In Play State as: " + user_data_->user_name());
		state_text_[1].setString("Press escape to quit.");

		// set up udp 'connections'
		sf::SocketSelector hosted_clients_selector;

		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			// initialise
			hosted_clients_udp_[i].Init(sf::Socket::AnyPort, connection_manager_tcp_->hosted_clients()[i].socket().getRemoteAddress(), connection_manager_tcp_->hosted_clients()[i].socket().getRemotePort());
			// add to selector
			hosted_clients_selector.add(hosted_clients_udp_[i].socket());
		}

		// Start Clients
		StartClients();
		timer->ResetGameTime();

		// wait for responses
		while (!AllClientsActive())
		{
			if (hosted_clients_selector.wait(sf::seconds(10)))
			{
				// received something
				for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
				{
					if (hosted_clients_selector.isReady(hosted_clients_udp_[i].socket()))
					{
						// this socket is ready, you can receive
						sf::Packet packet;
						hosted_clients_udp_[i].Receive(packet);
						// send same packet back right away
						hosted_clients_udp_[i].Send(packet);
						// this client can be considered active
						hosted_clients_udp_[i].set_active(true);
					}
				}
				for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
				{
					last_lagtime_update_[i] = timer->GameTime();
				}
			}
			else
			{
				// timeout reached, nothing was received...
				std::cout << "Time out reached for initial ping" << std::endl;
			}
		}

		// set all client sockets to non blocking
		for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
		{
			hosted_clients_udp_[i].SetSocketBlocking(false);
		}

	}

	void StatePlayHost::StartClients()
	{
		for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
		{
			string udp_port = std::to_string(hosted_clients_udp_[i].socket().getLocalPort());
			connection_manager_tcp_->hosted_clients()[i].SendMessageTCP(string("Start" + udp_port));
		}
	}

	bool StatePlayHost::AllClientsActive()
	{
		for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
		{
			if (!hosted_clients_udp_[i].active())
				return false;
		}
		return true;
	}

	void StatePlayHost::InitPlayers()
	{
		// Set up player data
		float player_size = 20.f;
		//float offset = (TILE_SIZE - player_size) * 0.5;
		float mid = 320;
		float low = 120;
		float high = 520;

		vector<sf::Vector2f> start_positions;
		vector<sf::Color> player_colours;

		switch (MAX_PLAYERS)
		{
		case 2:
			start_positions.push_back(sf::Vector2f(mid, high));
			start_positions.push_back(sf::Vector2f(mid, low));

			player_colours.push_back(sf::Color(0, 255, 255));
			player_colours.push_back(sf::Color(255, 0, 255));

			break;
		case 3:
			start_positions.push_back(sf::Vector2f(high, mid));
			start_positions.push_back(sf::Vector2f(low, mid));
			start_positions.push_back(sf::Vector2f(mid, high));

			player_colours.push_back(sf::Color(0, 255, 255));
			player_colours.push_back(sf::Color(255, 0, 255));
			player_colours.push_back(sf::Color(255, 255, 255));

			break;
		case 4:
			start_positions.push_back(sf::Vector2f(high, mid));
			start_positions.push_back(sf::Vector2f(low, mid));
			start_positions.push_back(sf::Vector2f(mid, low));
			start_positions.push_back(sf::Vector2f(mid, high));

			player_colours.push_back(sf::Color(0, 255, 255));
			player_colours.push_back(sf::Color(255, 0, 255));
			player_colours.push_back(sf::Color(0, 255, 0));
			player_colours.push_back(sf::Color(255, 255, 255));

			break;
		default:
			break;

		}

		// Init player models, views & controllers
		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			players_[i].Init(sf::Vector2f(player_size, player_size), start_positions[i]);
			player_views_[i].Init(&players_[i], player_colours[i]);
			
			if (i < user_data_->player_number())
			{
				predictive_controllers_[i].Init(&players_[i]);
			}
			else if (i > user_data_->player_number())
			{
				predictive_controllers_[i - 1].Init(&players_[i]);
			}
			else
			{
				user_player_controller_.Init(&players_[user_data_->player_number()]);
			}
		}

	}

	// Update the State (param[in] delta time)
	void StatePlayHost::StateUpdate(const float& dt, sf::RenderWindow* window)
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window->close();
				break;
			case sf::Event::TextEntered:
				// exit window on pressing escape
				if (event.text.unicode == 27)
				{
					if (next_state_type_ != EXIT) next_state_type_ = EXIT;
				}
				break;
			case sf::Event::KeyPressed:
				user_player_controller_.ReceivedKeyInput(event.key, true);
				break;
			case sf::Event::KeyReleased:
				user_player_controller_.ReceivedKeyInput(event.key, false);
				break;
			default:
				break;
			}
		}
		// update own player
		user_player_controller_.Update(dt);
		
		// check own physics
		physics_.Collision_BB_Tilemap(players_[user_data_->player_number()].bounding_box());

		// check for updates from clients
		for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
		{
			sf::Packet client_update;

			if (hosted_clients_udp_[i].Receive(client_update))
			{
				//std::cout << "got an update from client: " << std::endl;
				// now read the update
				ReadStateUpdate(client_update, timer_);
			}

			// update 'connection'
			hosted_clients_udp_[i].Update(dt);
			if (!hosted_clients_udp_[i].active())
			{
				if (state_text_[2].getString() == "")
				{
					state_text_[2].setString("Warning: Connection interrupted...");
				}
			}
		}

		// send updates
		// check state update timer
		if (state_update_timer_ < state_update_time_)
		{
			state_update_timer_ += dt;
		}
		else
		{
			// send update to clients
			SendStateUpdates();

			// zero timer
			state_update_timer_ = 0;
		}

		// update positions
		// update predictive controllers
		for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
		{
			predictive_controllers_[i].Update(timer_);
		}

		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			if (i != user_data_->player_number())
			physics_.Collision_BB_Tilemap(players_[i].bounding_box());
		}

		// update all views
		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			player_views_[i].Update();
		}
	}

	// compose and send a state update to all players
	void StatePlayHost::SendStateUpdates()
	{
		for (int i = 0; i < MAX_HOSTED_CLIENTS; ++i)
		{
			sf::Packet state_update;
			state_update << timer_->GameTime();
			for (sf::Int8 i = 0; i < MAX_PLAYERS; ++i)
			{
				state_update << i << players_[i].bounding_box().position().x << players_[i].bounding_box().position().y;
			}
			hosted_clients_udp_[i].Send(state_update);
		}
	}

	void StatePlayHost::ReadStateUpdate(sf::Packet& packet, Timer* timer)
	{
		// containers for data
		sf::Int64 time_stamp = 0;
		sf::Int8 player_id = 0;
		float pos_x = 0.f;
		float pos_y = 0.f;

		// read packet into containers
		if (packet >> time_stamp >> player_id >> pos_x >> pos_y)
		{
			// data read successfully
			//std::cout << "Received packet: " << time_stamp << ", " << player_id << ", " << pos_x << ", " << pos_y << std::endl;

			// create a position update
			PositionUpdate pos_update;
			pos_update.sent_time = time_stamp;
			pos_update.received_time = timer->GameTime();
			pos_update.position = sf::Vector2f(pos_x, pos_y);

			// check update player id
			if (player_id < user_data_->player_number())
			{
				predictive_controllers_[player_id].ReceivePositionUpdate(pos_update);
				// check lagtime update
				/*if (timer->GameTime() - last_lagtime_update_[player_id] > LAG_UPDATE)
				{
					sf::Packet lagtime_update;
					lagtime_update << time_stamp;
					hosted_clients_udp_[player_id].Send(lagtime_update);
					last_lagtime_update_[player_id] = timer->GameTime();
				}*/
			}
			// otherwise subtract one from index (so 'skip' us)
			else
			{
				predictive_controllers_[player_id - 1].ReceivePositionUpdate(pos_update);
				// check lagtime update
				/*if (timer->GameTime() - last_lagtime_update_[player_id - 1] > LAG_UPDATE)
				{
					sf::Packet lagtime_update;
					lagtime_update << time_stamp;
					hosted_clients_udp_[player_id].Send(lagtime_update);
					last_lagtime_update_[player_id - 1] = timer->GameTime();
				}*/
			}
		}
		else 
		{
			std::cout << "Failed to read packet" << std::endl;
		}
	}

	// Draw State (param[in] pointer to window)
	void StatePlayHost::StateRender(sf::RenderWindow* window)
	{

		map_view_.Render(window);

		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			player_views_[i].Render(window);
		}

		for (int i = 0; i < state_text_.size(); ++i)
		{
			window->draw(state_text_[i]);
		}
	}

	// Clean Up
	void StatePlayHost::StateCleanUp()
	{

	}

}