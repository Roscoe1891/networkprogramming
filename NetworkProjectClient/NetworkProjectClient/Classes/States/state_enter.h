#ifndef _STATE_ENTER_H
#define _STATE_ENTER_H

#include "state.h"

// The Enter State is where the player enters their name and connects to the server.

#define ENTER_STATE_TEXT 5

namespace NPC
{
	class StateEnter : public State
	{
	public:
		StateEnter();
		~StateEnter();
		// Intialise State
		virtual void StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer = nullptr);
		// Update the State (param[in] delta time)
		virtual void StateUpdate(const float& dt, sf::RenderWindow* window);
		// Draw State (param[in] pointer to window)
		virtual void StateRender(sf::RenderWindow* window);
		// Clean Up
		virtual void StateCleanUp();

	private:
		// Allows user to enter text
		void EnterText(sf::Event* text_event);
		// Utility function to access Connection Manager's function of same name
		void ConnectToServer();

		// used to hold user name
		string name_string_;

		// time (s) after which connection will time out
		const sf::Time connection_timeout_;
		
		// true if trying to connect right now
		bool connecting_;
		// true if connected to the server
		bool connected_;

		// allows creation of a dedicated thread for connection
		thread* connection_thread_;

		// mutex guards for shared resources
		mutex connecting_mutex_;
		mutex connected_mutex_;
		mutex state_text_mutex_;
		mutex name_string_mutex_;
	};

}

#endif _STATE_ENTER_H