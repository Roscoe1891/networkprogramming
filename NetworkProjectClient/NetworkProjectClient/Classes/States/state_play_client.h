#ifndef _STATE_PLAY_CLIENT_H
#define _STATE_PLAY_CLIENT_H

#include "state.h"
#include "GameObjects\player.h"
#include "GameObjectViews\game_object_view.h"
#include "GameObjectCtrls\player_controller.h"
#include "GameObjectCtrls\predictive_controller.h"
#include "Map\map_view.h"
#include "Network\connection_udp.h"
#include "Physics\physics_2d.h"

#define PLAY_STATE_TEXT_CLIENT 3

namespace NPC
{
	class StatePlayClient : public State
	{
	public:
		StatePlayClient();
		~StatePlayClient();
		// Intialise State
		virtual void StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer = nullptr);
		// Update the State (param[in] delta time)
		virtual void StateUpdate(const float& dt, sf::RenderWindow* window);
		// Draw State (param[in] pointer to window)
		virtual void StateRender(sf::RenderWindow* window);
		// Clean Up
		virtual void StateCleanUp();

	private:
		void InitPlayers();
		void UpdateLagTime(sf::Packet packet);
		void UpdateLagTime(sf::Int64 time_stamp);
		void SendStateUpdate();
		void ReadStateUpdate(sf::Packet& packet, Timer* timer);

		Timer* timer_;
		sf::Int64 lag_time_;

		float state_update_time_, state_update_timer_;

		Map map_;
		MapView map_view_;
		
		Player players_[MAX_PLAYERS];
		GameObjectView player_views_[MAX_PLAYERS];
		PredictiveController predictive_controllers_[MAX_PLAYERS - 1];
		PlayerController user_player_controller_;

		ConnectionUDP host_connection_;

		Physics2D physics_;

	};

}

#endif // !_STATE_PLAY_H