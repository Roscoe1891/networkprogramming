#include "state_manager.h"

namespace NPC
{
	StateManager::StateManager() : game_font_(nullptr), connection_manager_tcp_(nullptr), user_data_(nullptr), timer_(nullptr)
	{
	}

	StateManager::~StateManager()
	{
	}

	// Initialise State Manager
	void StateManager::Init(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer)
	{
		// set socket
		connection_manager_tcp_ = connection_manager;
		// set user data
		user_data_ = user_data;
		// set font
		game_font_ = font;
		// set timer 
		timer_ = timer;

		// populate states array
		game_states_[State::ENTER] = &state_enter_;
		game_states_[State::LOBBY] = &state_lobby_;
		game_states_[State::CONNECT_HOST] = &state_connect_host_;
		game_states_[State::CONNECT_CLIENT] = &state_connect_client_;

		game_states_[State::PLAY_HOST] = &state_play_host_;
		game_states_[State::PLAY_CLIENT] = &state_play_client_;

		game_states_[State::EXIT] = &state_exit_;

		// set current state
		current_state_ = State::ENTER;

		// Init current state
		game_states_[current_state_]->StateInit(connection_manager_tcp_, user_data_, game_font_);

	}

	// Update current state & check for state change
	void StateManager::StateUpdate(const float& dt, sf::RenderWindow* window)
	{
		// update current state
		game_states_[current_state_]->StateUpdate(dt, window);

		// if current state type doesn't match next state type
		if (current_state_ != game_states_[current_state_]->next_state_type())
		{
			ChangeState();	// change to next state
		}
	}

	// Render current state
	void StateManager::StateRender(sf::RenderWindow* window)
	{
		game_states_[current_state_]->StateRender(window);
	}

	// Change current state
	void StateManager::ChangeState()
	{
		// Clean up old state
		game_states_[current_state_]->StateCleanUp();
		// Set new current state
		current_state_ = game_states_[current_state_]->next_state_type();
		// Initialise new current state
		if ((current_state_ == State::PLAY_CLIENT) || (current_state_ == State::PLAY_HOST))
		{
			game_states_[current_state_]->StateInit(connection_manager_tcp_, user_data_, game_font_, timer_);
		}
		else
		{
			game_states_[current_state_]->StateInit(connection_manager_tcp_, user_data_, game_font_);
		}
	}
}
