#ifndef _STATE_MANAGER_H
#define _STATE_MANAGER_H

#include <SFML\Graphics.hpp>
#include "state.h"
#include "state_enter.h"
#include "state_lobby.h"
#include "state_connect_host.h"
#include "state_connect_client.h"
#include "state_play_host.h"
#include "state_play_client.h"
#include "state_exit.h"

namespace NPC
{
#define MAX_STATES 7

	class StateManager
	{
	public:

		StateManager();
		~StateManager();
		// Initialise State Manager
		void Init(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer);
		// Update current state
		void StateUpdate(const float& dt, sf::RenderWindow* window);
		// Render current state
		void StateRender(sf::RenderWindow* window);

		// return current state
		inline State* current_state() { return game_states_[current_state_]; }

	private:
		// Change current state
		void ChangeState();

		// create one of each state
		StateEnter state_enter_;
		StateLobby state_lobby_;
		StateConnectHost state_connect_host_;
		StateConnectClient state_connect_client_;
		StatePlayHost state_play_host_;
		StatePlayClient state_play_client_;
		StateExit state_exit_;

		State* game_states_[MAX_STATES];					// provides index reference to states
		State::StateType current_state_;					// enumeration of the current state

		ConnectionManagerTCP* connection_manager_tcp_;		// the socket for connecting to the server
		sf::Font* game_font_;								// the main font used for the game

		UserData* user_data_;								// the user data

		Timer* timer_;										// the game timer

	};
}

#endif