#ifndef _STATE_CONNECT_CLIENT_H
#define _STATE_CONNECT_CLIENT_H

#include "state.h"
#include <thread>
#include <mutex>

#define CONNECT_CLIENT_STATE_TEXT 1

// A client Connect state simply establishes a connection to the server before progressing to the play state

namespace NPC
{
	using std::thread;

	class StateConnectClient : public State
	{
	public:
		StateConnectClient();
		~StateConnectClient();

		// Intialise State
		virtual void StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer = nullptr);
		// Update the State (param[in] delta time)
		virtual void StateUpdate(const float& dt, sf::RenderWindow* window);
		// Draw State (param[in] pointer to window)
		virtual void StateRender(sf::RenderWindow* window);
		// Clean Up
		virtual void StateCleanUp();

	private:
		// State thread function for clients
		void Connect();

		thread* connect_thread_;
		bool connected_, connecting_;
		mutex connected_mutex_, connecting_mutex_;
 

	

	};
}

#endif // !_STATE_CONNECT_H
