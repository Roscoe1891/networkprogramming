#ifndef _STATE_EXIT_H
#define _STATE_EXIT_H

#include "state.h"

namespace NPC
{
	class StateExit : public State
	{
	public:
		StateExit();
		~StateExit();

		// Intialise State
		virtual void StateInit(ConnectionManagerTCP* connection_manager, UserData* user_data, sf::Font* font, Timer* timer = nullptr);
		// Update the State (param[in] delta time)
		virtual void StateUpdate(const float& dt, sf::RenderWindow* window);
		// Draw State (param[in] pointer to window)
		virtual void StateRender(sf::RenderWindow* window);
		// Clean Up
		virtual void StateCleanUp();

	private:

	};
}

#endif