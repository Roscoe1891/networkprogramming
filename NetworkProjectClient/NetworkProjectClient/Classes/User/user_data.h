#ifndef _USER_DATA_H
#define _USER_DATA_H

#include <string>
#include "SFML\Network.hpp"

using std::string;

namespace NPC
{
	class UserData
	{
	public:
		UserData();
		~UserData();

		inline const string& user_name() const { return user_name_; }
		inline void set_user_name(string& new_name) { user_name_ = new_name; }

		inline const bool& host() const { return host_; }
		inline void set_host(bool new_value) { host_ = new_value; }

		inline const sf::Int8 player_number() const { return player_number_; }
		inline void set_player_number(sf::Int8 new_number) { player_number_ = new_number; }

	private:
		string user_name_;
		bool host_;
		sf::Int8 player_number_;

	};
}

#endif