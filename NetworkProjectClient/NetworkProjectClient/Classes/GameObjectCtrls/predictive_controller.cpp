#include "predictive_controller.h"

namespace NPC
{
	PredictiveController::PredictiveController() : c_max_position_updates_(3)
	{
	}

	PredictiveController::~PredictiveController()
	{
	}

	void PredictiveController::Init(Player* player)
	{
		player_ = player;
	}

	void PredictiveController::Update(Timer* time)
	{
		if (player_ != nullptr)
		{
			//std::cout << "My player is at " << player_ << std::endl;
			//std::cout << "Position updates has " << position_updates_.size() << "updates in it" << std::endl;

			// for position updates, back of the list is the most recent
			sf::Vector2f new_pos;
			
			// simplistic
			if (position_updates_.size() == 1)
			{
				new_pos = position_updates_.back().position;
				player_->bounding_box().set_position(new_pos);
				//std::cout << "New position is: " << player_->bounding_box().position().x << ", "  << player_->bounding_box().position().y << std::endl;
			}

			// predictive
			// calc position NOW based on position updates 1 and 2
			else if (position_updates_.size() == 2)
			{
				PositionUpdate* update1 = nullptr;
				PositionUpdate* update2 = nullptr;

				std::list<NPC::PositionUpdate>::iterator it = position_updates_.end();
				it--;
				update1 = &(*it);
				it--;
				update2 = &(*it);

				new_pos = PredictPosition(update1, update2, time->GameTime());

				player_->bounding_box().set_position(new_pos);
			}

			// interpolative
			// calc position NOW based on position updates 0 and 1
			// calc position last update based on position updates 1 and 2
			// lerp between these values

			else if (position_updates_.size() >= 3)
			{
				sf::Vector2f predicted_pos_A;
				sf::Vector2f predicted_pos_B;
				sf::Vector2f new_pos;

				PositionUpdate* update1 = nullptr;
				PositionUpdate* update2 = nullptr;
				PositionUpdate* update3 = nullptr;

				std::list<NPC::PositionUpdate>::iterator it = position_updates_.end();
				it--;
				update1 = &(*it);
				it--;
				update2 = &(*it);
				it--;
				update3 = &(*it);

				// Get predictions
				predicted_pos_A = PredictPosition(update2, update3, update1->received_time);
				predicted_pos_B = PredictPosition(update1, update2, time->GameTime());

				// Interpolate between predictions
				float interp_value = GetInterpolationValue(time->GameTime(), update1->received_time, update2->received_time);
				if ((interp_value > 0) && (interp_value < 1)) std::cout << interp_value << std::endl;
				new_pos = InterpolatePosition(predicted_pos_A, predicted_pos_B, interp_value);

				player_->bounding_box().set_position(new_pos);
			}
		}
		else {
			std::cout << "Player is null pointer" << std::endl;
		}	
	}

	float PredictiveController::GetInterpolationValue(sf::Int64 current_time, sf::Int64 time1, sf::Int64 time2)
	{
		float result = 0;

		result = ((current_time - time1) / (time1 - time2));

		// clamp between 0 & 1
		if (result < 0) result = 0;
		if (result > 1) result = 1;

		//std::cout << "Interp Value: " << result << std::endl;

		return result;
	}


	sf::Vector2f PredictiveController::InterpolatePosition(sf::Vector2f position_A, sf::Vector2f position_B, float interp_value)
	{
		sf::Vector2f result;

		result.x = ((1 - interp_value) * position_A.x) + (interp_value * position_B.x);
		result.y = ((1 - interp_value) * position_A.y) + (interp_value * position_B.y);

		return result;

	}

	// Return position at current time based on the supplied position updates (where update 1 is the most recent)
	sf::Vector2f PredictiveController::PredictPosition(PositionUpdate* update1, PositionUpdate* update2, sf::Int64 current_time)
	{
		sf::Vector2f result;
		float x1 = update1->position.x;
		float y1 = update1->position.y;
		sf::Int64 t1 = update1->sent_time;

		float x2 = update2->position.x;
		float y2 = update2->position.y;
		sf::Int64 t2 = update2->sent_time;

		sf::Int64 t = t1 - t2;

		float x = (x1 - x2) / t;
		float y = (y1 - y2) / t;

		sf::Int64 time_diff = current_time - update1->sent_time;

		result.x = update1->position.x + (x * time_diff);
		result.y = update1->position.y + (y * time_diff);

		return result;
	}

	void PredictiveController::ReceivePositionUpdate(PositionUpdate update)
	{
		// add update to the list
		InsertOrdered(update);
		// shorten list if necessary
		while (position_updates_.size() > c_max_position_updates_)
		{
			position_updates_.pop_front();
		}
	}

	void PredictiveController::InsertOrdered(PositionUpdate update)
	{
		for (auto it = position_updates_.begin(); it != position_updates_.end(); ++it)
		{
			// if it's time value is earlier than our update
			if ((*it).sent_time < update.sent_time)
			{
				// add update to list before it
				position_updates_.insert(it, update);
				break;
			}
		}
		// if reached end of list, add to end
		position_updates_.push_back(update);

	}

}