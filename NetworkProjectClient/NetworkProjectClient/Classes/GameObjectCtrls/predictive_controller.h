#ifndef _PREDICTIVE_CONTROLLER_H
#define _PREDICTIVE_CONTROLLER_H

#include "GameObjects\player.h"
#include "Utilities\timer.h"
#include <iostream>
#include <list>

namespace NPC
{
	using std::list;

	struct PositionUpdate
	{
		sf::Vector2f position;
		sf::Int64 sent_time;
		sf::Int64 received_time;
	};

	class PredictiveController
	{
	public:
		PredictiveController();
		~PredictiveController();

		void Init(Player* player);
		void Update(Timer* time);
		void ReceivePositionUpdate(PositionUpdate update);
		void InsertOrdered(PositionUpdate update);

	private:
		// Return position at current time based on the supplied position updates (where update 1 is the most recent)
		sf::Vector2f PredictPosition(PositionUpdate* update1, PositionUpdate* update2, sf::Int64 current_time);
		// Calculate and return interpolation value for use in an Interpolate Position operation
		float GetInterpolationValue(sf::Int64 current_time, sf::Int64 time1, sf::Int64 time2);
		// Return position interpolated between the supplied positions by the supplied interpolation value (0 = position A, 1 = position B)
		sf::Vector2f InterpolatePosition(sf::Vector2f position_A, sf::Vector2f position_B, float interp_value);

		Player* player_;
		list<PositionUpdate> position_updates_;
		const int c_max_position_updates_;
	};
}

#endif