#include "player_controller.h"

namespace NPC
{
	PlayerController::PlayerController() : player_(nullptr)
	{
	}

	PlayerController::~PlayerController()
	{
	}

	void PlayerController::Init(Player* player)
	{
		player_ = player;
	}

	void PlayerController::Update(const float& dt)
	{
		// Move player
		if ((player_->velocity().x != 0) || (player_->velocity().y != 0))
		{
			player_->Move(dt);
		}
	}

	void PlayerController::ReceivedKeyInput(sf::Event::KeyEvent key_input, bool key_pressed)
	{
		if (key_pressed)
		{
			switch (key_input.code)
			{
			case sf::Keyboard::W:	// UP
				player_->set_velocity(sf::Vector2f(player_->velocity().x, -player_->move_speed()));
				break;
			case sf::Keyboard::S:	// DOWN
				player_->set_velocity(sf::Vector2f(player_->velocity().x, player_->move_speed()));
				break;
			case sf::Keyboard::A:	// LEFT
				player_->set_velocity(sf::Vector2f(-player_->move_speed(), player_->velocity().y));
				break;
			case sf::Keyboard::D:	// RIGHT
				player_->set_velocity(sf::Vector2f(player_->move_speed(), player_->velocity().y));
				break;
			}
		}
		else
		{
			switch (key_input.code)
			{
			case sf::Keyboard::W:	// UP
				player_->set_velocity(sf::Vector2f(player_->velocity().x, 0));
				break;
			case sf::Keyboard::S:	// DOWN
				player_->set_velocity(sf::Vector2f(player_->velocity().x, 0));
				break;
			case sf::Keyboard::A:	// LEFT
				player_->set_velocity(sf::Vector2f(0, player_->velocity().y));
				break;
			case sf::Keyboard::D:	// RIGHT
				player_->set_velocity(sf::Vector2f(0, player_->velocity().y));
				break;
			}
		}
	}
}