#ifndef _PLAYER_CONTROLLER_H
#define _PLAYER_CONTROLLER_H

#include "GameObjects\player.h"
#include "SFML\Window.hpp"

namespace NPC
{
	class PlayerController
	{
	public:
		PlayerController();
		~PlayerController();

		void Init(Player* player);
		void ReceivedKeyInput(sf::Event::KeyEvent key_input, bool key_pressed);
		void Update(const float& dt);

	private:
		Player* player_;
	};

}

#endif
