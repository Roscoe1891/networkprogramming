#ifndef TIMER_H
#define TIMER_H

#include <SFML\System.hpp>

// The Timer class measures the total time the application has been running (game time).
// It also allows access to delta time

namespace NPC
{
	class Timer
	{
	public:
		Timer();
		~Timer();
		void Update();
		void ResetGameTime();

		float DeltaTime();
		sf::Int64 GameTime();

	private:

		sf::Clock clock_;
		sf::Time old_time_;
		sf::Time delta_time_;
		sf::Int64 lag_time_;

	};
}



#endif // !TIMER_H
