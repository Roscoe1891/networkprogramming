#include "Utilities\timer.h"

namespace NPC
{
	Timer::Timer()
	{
		old_time_ = sf::microseconds(0);
		delta_time_ = sf::microseconds(0);
	}

	Timer::~Timer()
	{
	}

	void Timer::Update()
	{
		delta_time_ = clock_.getElapsedTime() - old_time_;
		old_time_ = clock_.getElapsedTime();
	}

	float Timer::DeltaTime()
	{
		return delta_time_.asSeconds();
	}

	void Timer::ResetGameTime()
	{
		clock_.restart();
		old_time_ = sf::microseconds(0);
	}

	sf::Int64 Timer::GameTime()
	{
		return clock_.getElapsedTime().asMicroseconds();
	}
}
