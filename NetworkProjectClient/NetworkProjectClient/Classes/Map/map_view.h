#ifndef _MAP_VIEW_H
#define _MAP_VIEW_H

#include "SFML\Graphics.hpp"
#include "map.h"

namespace NPC
{


	class MapView
	{
	public:
		MapView();
		~MapView();

		void Init(Map* map);
		void Render(sf::RenderWindow* window);

	private:
		// The map this view will draw
		Map* map_;
		sf::RectangleShape clear_tile_;
		sf::RectangleShape blocked_tile_;


	};
}

#endif