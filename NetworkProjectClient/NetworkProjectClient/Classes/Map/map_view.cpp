#include "map_view.h"

namespace NPC
{
	MapView::MapView()
	{
	}

	MapView::~MapView()
	{
	}

	void MapView::Init(Map* map)
	{
		map_ = map;

		clear_tile_.setSize(sf::Vector2f(TILE_SIZE, TILE_SIZE));
		clear_tile_.setFillColor(sf::Color(70, 70, 75));

		blocked_tile_.setSize(sf::Vector2f(TILE_SIZE, TILE_SIZE));
		blocked_tile_.setFillColor(sf::Color(40, 40, 45));
		blocked_tile_.setOutlineThickness(-TILE_BORDER);
		blocked_tile_.setOutlineColor(sf::Color(20, 0, 35));

	}

	void MapView::Render(sf::RenderWindow* window)
	{
		float half_tile_size = TILE_SIZE * 0.5;

		sf::Vector2f tile_pos = sf::Vector2f((-TILE_SIZE -half_tile_size), (-TILE_SIZE -half_tile_size));

		for (int y = 0; y < MAP_HEIGHT; ++y)
		{
			tile_pos.y += TILE_SIZE;
			tile_pos.x = (-TILE_SIZE - half_tile_size);

			for (int x = 0; x < MAP_WIDTH; ++x)
			{
				tile_pos.x += TILE_SIZE;

				switch (map_->floor_plan()[y][x])
				{
				case TileType::BLOCKED:
					blocked_tile_.setPosition(tile_pos);
					window->draw(blocked_tile_);
					break;
				case TileType::CLEAR:
					clear_tile_.setPosition(tile_pos);
					window->draw(clear_tile_);
					break;
				}
			}
		}
	}

}