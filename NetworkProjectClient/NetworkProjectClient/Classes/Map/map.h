#ifndef _MAP_H
#define _MAP_H

#define MAP_HEIGHT 17
#define MAP_WIDTH 17
#define TILE_SIZE 40
#define TILE_BORDER 5

#include <iostream>
#include "SFML/System.hpp"

typedef int FloorPlan[MAP_HEIGHT][MAP_WIDTH];

namespace NPC
{
	using sf::Vector2i;

	enum TileType { CLEAR, BLOCKED };

	class Map
	{
	public:
		Map();
		~Map();

		// Return floor plan
		inline const FloorPlan& floor_plan() const { return floor_plan_; }

		// Initialise map
		void Init();

		// Param in point to get tile type
		TileType GetTileTypeAtMapCoord(Vector2i map_coord);

	private:
		// The floor plan for this map
		FloorPlan floor_plan_;
		
		// Copy source floorplan into destination floor plan
		void CopyFloorPlan(FloorPlan& source, FloorPlan dest);
	};
}

#endif // !MAP_H
