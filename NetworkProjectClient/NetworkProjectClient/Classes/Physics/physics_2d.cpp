#include "physics_2d.h"

namespace NPC
{
	Physics2D::Physics2D()
	{
	}

	Physics2D::~Physics2D()
	{
	}

	void Physics2D::Init(Map* map)
	{
		map_ = map;
	}

	// return tile type
	void Physics2D::Collision_BB_Tilemap(BoundingBox& bb)
	{
		// 1: Create and fill stack containing occupied tiles
		stack<Vector2i> occupiedTiles;
		GetOccupiedTiles(bb, occupiedTiles);
		// 2: Resolve collision against occupied tiles
		Collide_BB_OccupiedTiles(bb, occupiedTiles);
	}

	// Param[in] stack to be populated with tiles occupied by bounding box
	void Physics2D::GetOccupiedTiles(const BoundingBox& bb, stack<Vector2i>& occTiles)
	{
		// centre tile
		Vector2i centreTile = Convert_WorldToMap(bb.position());
		occTiles.push(centreTile);

		// Get overlap tiles:
		// define offsets 
		Vector2f occTilePos = Convert_MapToWorld(centreTile);
		float offsetX = bb.position().x - occTilePos.x;
		float offsetY = bb.position().y - occTilePos.y;

		// define allowance  
		float allowanceX = TILE_SIZE * 0.5 - bb.half_width();
		float allowanceY = TILE_SIZE * 0.5 - bb.half_height();

		// x overlap tile
		Vector2i xOverlapTile = GetOverlapTile_X(offsetX, allowanceX, centreTile);
		if ((xOverlapTile.x != 0) && (xOverlapTile.y != 0))
			occTiles.push(xOverlapTile);
		// y overlap tile
		Vector2i yOverlapTile = GetOverlapTile_Y(offsetY, allowanceY, centreTile);
		if ((yOverlapTile.x != 0) && (yOverlapTile.y != 0))
			occTiles.push(yOverlapTile);
		// diagonal overlap tile
		Vector2i diagonalOverlapTile = GetOverlapTile_Diagonal(offsetX, offsetY, allowanceX, allowanceY, centreTile);
		if ((diagonalOverlapTile.x != 0) && (diagonalOverlapTile.y != 0))
			occTiles.push(diagonalOverlapTile);
	}

	// Return overlapping tile co-ordinate (x axis)
	Vector2i Physics2D::GetOverlapTile_X(const float& offset, const float& allowance, const Vector2i& map_coords)
	{
		Vector2i overlap = Vector2i(map_coords.x, map_coords.y);
		if (offset > allowance) // if object is right of centre 
		{
			overlap.x += 1;
		}
		else if (offset < allowance * -1) // if object is left of centre  
		{
			overlap.x -= 1;
		}
		else                                // if neither
		{
			return Vector2i(0, 0);					// return invalid location 
		}
		return overlap;	// return the tile at the point if found
	}
	
	// Return overlapping tile co-ordinate (y axis)
	Vector2i Physics2D::GetOverlapTile_Y(const float& offset, const float& allowance, const Vector2i& map_coords)
	{
		Vector2i overlap = Vector2i(map_coords.x, map_coords.y);
		if (offset > allowance)				// if object is below centre 
		{
			overlap.y += 1;
		}
		else if (offset < allowance * -1)	// if object is above centre  
		{
			overlap.y -= 1;
		}
		else                                // if neither
		{
			return Vector2i(0, 0);					// return invalid location 
		}
		return overlap;	// return the tile at the point if found
	}
	
	// Return overlapping tile co-ordinate (diagonal)
	Vector2i Physics2D::GetOverlapTile_Diagonal(const float& offsetX, const float& offsetY, const float& allowanceX, const float& allowanceY, const Vector2i& map_coords)
	{
		Vector2i overlap = Vector2i(map_coords.x, map_coords.y);
		if ((offsetX > allowanceX) && (offsetY < allowanceY * -1))				// if right and above centre 
		{
			overlap.x += 1;
			overlap.y -= 1;
		}
		else if ((offsetX > allowanceX) && (offsetY > allowanceY))				// if right and below centre 
		{
			overlap.x += 1;
			overlap.y += 1;
		}
		else if ((offsetX > allowanceX) && (offsetY > allowanceY))				// if right and below centre 
		{
			overlap.x += 1;
			overlap.y += 1;
		}
		else if ((offsetX < allowanceX * -1) && (offsetY < allowanceY * -1))	// if left and above centre 
		{
			overlap.x -= 1;
			overlap.y -= 1;
		}
		else if ((offsetX < allowanceX * -1) && (offsetY > allowanceY))			// if left and below centre 
		{
			overlap.x -= 1;
			overlap.y += 1;
		}
		else
		{
			return Vector2i(0, 0);					// return invalid location 
		}
		return overlap;	// return the tile at the point if found
	}

	// param[in] occupied tile stack to check
	void Physics2D::Collide_BB_OccupiedTiles(BoundingBox& bb, stack<Vector2i>& occTiles)
	{
		while (!occTiles.empty())
		{
			// check tile type 
			if (map_->GetTileTypeAtMapCoord(occTiles.top()) == BLOCKED)	// if tile is blocked
			{
				// project out of collision
				Project_BB_Tile(bb, occTiles.top());
			}
			// remove tle from stack
			occTiles.pop();
		}
	}


	// project object out of collision with provided point (i.e. tile on map)
	void Physics2D::Project_BB_Tile(BoundingBox& bb, Vector2i& map_coords)
	{
		// get point's real world position
		Vector2f map_coords_position = Convert_MapToWorld(map_coords);
		// get calculation values
		float halfw1 = bb.half_width();
		float halfh1 = bb.half_height();
		float halfw2 = TILE_SIZE * 0.5;
		float halfh2 = TILE_SIZE * 0.5;
		float posx1 = bb.position().x;
		float posy1 = bb.position().y;
		float posx2 = map_coords_position.x;
		float posy2 = map_coords_position.y;
		// calc x & y difference (vector direction)
		float posYDiff = posy1 - posy2;
		float posXDiff = posx1 - posx2;
		// calc overlap (vector magnitude)
		float overlapX = halfw1 + halfw2 - abs(posXDiff);
		float overlapY = halfh1 + halfh2 - abs(posYDiff);

		// if the x overlap is the lesser
		if (overlapX < overlapY)
		{
			float newPosX = 0;
			// if object 1 is left of object 2
			if (posXDiff < 0)
			{
				// projection vector is negative
				newPosX = posx1 - overlapX - 1;
			}
			else
			{
				// projection vector is positive
				newPosX = posx1 + overlapX + 1;
			}
			bb.set_position(newPosX, posy1);
			bb.owner()->set_velocity(Vector2f(0, 0));
		}
		else	// if the y overlap is the lesser
		{
			float newPosY = 0;
			// if object 1 is above object 2
			if (posYDiff < 0)
			{
				// projection vector is negative
				newPosY = posy1 - overlapY - 1;
			}
			else
			{
				// projection vector is positive
				newPosY = posy1 + overlapY + 1;
			}
			bb.set_position(posx1, newPosY);
			bb.owner()->set_velocity(Vector2f(0, 0));

		}
	}

	// param[in] world position to be returned as a map co-ord
	Vector2i Physics2D::Convert_WorldToMap(Vector2f world_position)
	{
		int xPos = (world_position.x + (TILE_SIZE * 0.5)) / TILE_SIZE;
		int yPos = (world_position.y + (TILE_SIZE * 0.5)) / TILE_SIZE;
		Vector2i map_coord = Vector2i(xPos, yPos);
		return map_coord;
	}
	
	// param[in] map co-ord to be returned as a world position
	Vector2f Physics2D::Convert_MapToWorld(Vector2i map_coord)
	{
		//int halfTileSize = 0;// TILE_SIZE * 0.5;
		Vector2f world_pos = Vector2f((map_coord.x * TILE_SIZE), map_coord.y * TILE_SIZE);
		return world_pos;
	}
}

