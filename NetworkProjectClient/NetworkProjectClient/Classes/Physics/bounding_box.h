#ifndef _BOUNDING_BOX_H
#define _BOUNDING_BOX_H

#include "SFML\System.hpp"

namespace NPC
{
	using sf::Vector2f;
	class GameObject;

	class BoundingBox
	{
	public:
		BoundingBox();
		~BoundingBox();
		void Init(GameObject* owner, float width, float height, float x, float y);

		const float EdgeTop() const;				// returns x position of top edge
		const float EdgeBottom() const;				// returns x position of bottom edge
		const float EdgeLeft() const;				// returns y position of left edge
		const float EdgeRight() const;				// returns y position of right edge
		const Vector2f VertexTL() const;			// returns position of Top Left Vertex
		const Vector2f VertexTR() const;			// returns position of Top Right Vertex
		const Vector2f VertexBL() const;			// returns position of Bottom Left Vertex
		const Vector2f VertexBR() const;			// returns position of Bottom Right Vertex

		// return pointer to owner
		inline GameObject* owner() { return owner_; }

		// @param[in] the position of the bounding box
		inline void set_position(const Vector2f& position) { position_ = position; }
		inline void set_position(const float x, const float y) { position_.x = x; position_.y = y; }

		// @ return width of the bounding box
		inline const float& width() const { return width_; }
		// @ return height of the bounding box
		inline const float& height() const { return height_; }
		// @ return half width of the bounding box
		inline const float& half_width() const { return half_width_; }
		// @ return half height of the bounding box
		inline const float& half_height() const { return half_height_; }
		// @return the position of the bounding box
		inline const Vector2f& position() const { return position_; }

	private:
		GameObject* owner_;			// the game object associated with this bounding box
		Vector2f position_;			// bounding box position
		float width_;				// bounding box width
		float height_;				// bounding box height
		float half_width_;			// bounding box 1/2 width
		float half_height_;			// bounding box 1/2 height
		
	};
}

#endif // !_BOUNDING_BOX_H
