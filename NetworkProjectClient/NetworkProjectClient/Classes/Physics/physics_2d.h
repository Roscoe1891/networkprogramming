#ifndef _PHYSICS_2D_H
#define _PHYSICS_2D_H

// Max number of tiles a single object can occupy
#define MAX_OCCUPIED_TILES 4
// Total tiles of one tile + surrounding tiles
#define MAX_NEIGHBOURS 4

#include "GameObjects\game_object.h"
#include "bounding_box.h"
#include "Map\map.h"
#include <stack>

namespace NPC
{
	using std::stack;
	using sf::Vector2i;
	typedef sf::Vector2i TileNeighbours[MAX_NEIGHBOURS];

	class Physics2D
	{
	public:
		Physics2D();
		~Physics2D();
		
		void Init(Map* map);

		// param[in] bb to check vs tilemap
		void Collision_BB_Tilemap(BoundingBox& bb);
		

	private:
		// TILEMAP VS BOUNDING BOX COLLISION FUNCTIONS:
		// Get Tile Co Ordinates
		void GetOccupiedTiles(const BoundingBox& bb, stack<Vector2i>& occTiles);
		Vector2i GetOverlapTile_X(const float& offset, const float& allowance, const Vector2i& map_coords);
		Vector2i GetOverlapTile_Y(const float& offset, const float& allowance, const Vector2i& map_coords);
		Vector2i GetOverlapTile_Diagonal(const float& offsetX, const float& offsetY, const float& allowanceX, const float& allowanceY, const Vector2i& map_coords);
		// param[in] occupied tile stack to check
		void Collide_BB_OccupiedTiles(BoundingBox& bb, stack<Vector2i>& occTiles);
		// project object out of collision with provided point (i.e. tile on map)
		void Project_BB_Tile(BoundingBox& bb, Vector2i& map_coords);

		// CONVERSIONS
		// param[in] world position to be returned as a map co-ord
		Vector2i Convert_WorldToMap(Vector2f world_position);
		// param[in] map co-ord to be returned as a world position
		Vector2f Convert_MapToWorld(Vector2i map_coord);

		// The game map
		Map* map_;
	};

}

#endif