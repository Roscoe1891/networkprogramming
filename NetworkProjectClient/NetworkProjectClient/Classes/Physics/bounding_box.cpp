#include "bounding_box.h"

namespace NPC
{

	BoundingBox::BoundingBox() : owner_(nullptr)
	{
		position_ = Vector2f(0.f, 0.f);
		width_ = 0.f;
		height_ = 0.f;
		half_width_ = 0.f;
		half_height_ = 0.f;
	}

	BoundingBox::~BoundingBox()
	{
	}

	void BoundingBox::Init(GameObject* owner, float width, float height, float x, float y)
	{
		owner_ = owner;
		width_ = width;
		height_ = height;
		half_height_ = height*0.5;
		half_width_ = width*0.5;
		position_ = Vector2f(x, y);
	}

	// returns x position of top edge
	const float BoundingBox::EdgeTop() const
	{
		return position_.y - half_height_;
	}

	// returns x position of bottom edge
	const float BoundingBox::EdgeBottom() const
	{
		return position_.y + half_height_;
	}

	// returns y position of left edge
	const float BoundingBox::EdgeLeft() const
	{
		return position_.x - half_width_;
	}

	// returns y position of right edge
	const float BoundingBox::EdgeRight() const
	{
		return position_.x + half_width_;
	}

	// returns position of Top Left Vertex
	const Vector2f BoundingBox::VertexTL() const
	{
		float x = position_.x - half_width_;
		float y = position_.y - half_height_;
		Vector2f vertex = Vector2f(x, y);
		return vertex;
	}

	// returns position of Top Right Vertex
	const Vector2f BoundingBox::VertexTR() const
	{
		float x = position_.x + half_width_;
		float y = position_.y - half_height_;
		Vector2f vertex = Vector2f(x, y);
		return vertex;
	}

	// returns position of Bottom Left Vertex
	const Vector2f BoundingBox::VertexBL() const
	{
		float x = position_.x - half_width_;
		float y = position_.y + half_height_;
		Vector2f vertex = Vector2f(x, y);
		return vertex;
	}

	// returns position of Bottom Right Vertex
	const Vector2f BoundingBox::VertexBR() const
	{
		float x = position_.x + half_width_;
		float y = position_.y + half_height_;
		Vector2f vertex = Vector2f(x, y);
		return vertex;
	}

}