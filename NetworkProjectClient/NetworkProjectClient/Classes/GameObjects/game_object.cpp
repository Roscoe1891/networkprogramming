#include "game_object.h"

namespace NPC
{
	GameObject::GameObject()
	{
		velocity_ = sf::Vector2f(0, 0);
	}

	GameObject::~GameObject()
	{
	}

	void GameObject::Move(const float& dt)
	{
		Vector2f new_pos;
		new_pos.x = bounding_box_.position().x + (velocity_.x * dt);
		new_pos.y = bounding_box_.position().y + (velocity_.y * dt);

		bounding_box_.set_position(new_pos);
	}

}