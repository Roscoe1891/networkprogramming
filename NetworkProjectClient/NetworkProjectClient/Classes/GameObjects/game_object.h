#ifndef _GAME_OBJECT_H
#define _GAME_OBJECT_H

#include "SFML\System.hpp"
#include "Physics\bounding_box.h"

namespace NPC
{
	class GameObject
	{
	public:
		GameObject();
		~GameObject();

		// return velocity
		inline const sf::Vector2f& velocity() const { return velocity_; }
		// param[in] new velocity
		inline void set_velocity(sf::Vector2f new_vel) { velocity_ = new_vel; }

		// return reference to bounding box
		inline BoundingBox& bounding_box() { return bounding_box_; }

		// Initialise
		virtual void Init(sf::Vector2f size, sf::Vector2f position) = 0;
		// Update
		virtual void Update(const float& dt) = 0;
		// Move
		virtual void Move(const float& dt);

	protected:
		sf::Vector2f velocity_;

		BoundingBox bounding_box_;
	};
}

#endif // !_GAME_OBJECT_H
