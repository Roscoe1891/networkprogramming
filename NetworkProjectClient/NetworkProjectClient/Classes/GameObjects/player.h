#ifndef _PLAYER_H
#define _PLAYER_H

#include "game_object.h"

namespace NPC
{
	class Player : public GameObject
	{
	public:
		Player();
		~Player();

		inline const float& move_speed() const { return move_speed_; } 

		// Initialise
		virtual void Init(sf::Vector2f size, sf::Vector2f position);
		// Update
		virtual void Update(const float& dt);


	private:
		const float move_speed_;


	};

}

#endif