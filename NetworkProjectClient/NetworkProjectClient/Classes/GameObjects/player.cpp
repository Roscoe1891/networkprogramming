#include "player.h"

namespace NPC
{

	Player::Player() : move_speed_(200)
	{
	}

	Player::~Player()
	{
	}

	// Initialise
	void Player::Init(sf::Vector2f size, sf::Vector2f position)
	{
		bounding_box_.Init(this, size.x, size.y, position.x, position.y);
	}

	// Update
	void Player::Update(const float& dt)
	{

	}

}