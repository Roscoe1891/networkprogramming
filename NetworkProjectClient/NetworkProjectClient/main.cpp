// Network Project: Client

#include <iostream>
#include <SFML\Graphics.hpp>
#include "Classes\Network\connection_manager_tcp.h"
#include "Classes\Utilities\timer.h"
#include "Classes\States\state_manager.h"
#include "Classes\User\user_data.h"

#define WINDOW_HEIGHT 640
#define WINDOW_WIDTH 640

	int main()
	{
		std::cout << "Network Project: Client\n" << std::endl;

		// Load external files:
		sf::Font font_;
		if (!font_.loadFromFile("Bin/01logistix.ttf"))
		{
			// handle error
			std::cout << "Error loading font file 01logistix" << std::endl;
		}

		// Create Connection Manager
		NPC::ConnectionManagerTCP connection_manager_tcp_;

		// Create timer
		NPC::Timer timer_;

		// Create User Data
		NPC::UserData user_data_;

		// Create State Manager
		NPC::StateManager state_manager_;
		// Initialise State Manager
		state_manager_.Init(&connection_manager_tcp_, &user_data_, &font_, &timer_);

		// Create application window
		sf::RenderWindow window_(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Client window", sf::Style::Titlebar | sf::Style::Close);

		while (window_.isOpen())
		{
			// get delta time
			timer_.Update();

			// Update State
			state_manager_.StateUpdate(timer_.DeltaTime(), &window_);

			// Draw calls
			window_.clear();

			// Draw current state 
			state_manager_.StateRender(&window_);

			window_.display();
		}

		return 0;
	
}

