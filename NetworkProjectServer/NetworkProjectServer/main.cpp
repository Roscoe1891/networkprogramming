// Network Project: Server

#include <iostream>
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include <list>
#include <thread>
#include <mutex>

#include "Utilities\timer.h"
#include "Network\connection_manager.h"

using std::list;
using std::thread;
using std::mutex;

// Prototype functions:
void DrawConnections(sf::RenderWindow* window, list<NPS::ConnectionTCP*>* clients, sf::Text* text);

int main()
{
	std::cout << "Network Project: Server\n" << std::endl;

	// Load External Files
	sf::Font font_;
	if (font_.loadFromFile("Bin/Roboto-Regular.ttf"))
	{
		// handle error
	}
	sf::Text text_;
	text_.setFont(font_);
	text_.setString("Network Server Project.");

	// Create timer
	NPS::Timer timer_;

	// Create the connection manager
	NPS::ConnectionManager connection_manager_;
	connection_manager_.Init();

	// Create application window
	sf::RenderWindow window_(sf::VideoMode(800, 600), "Server window", sf::Style::Titlebar | sf::Style::Close);

	while (window_.isOpen())
	{
		// Check events
		sf::Event event;
		while (window_.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				//connection_manager_.CleanUp();
				window_.close();
			}
		}

		// get delta time
		timer_.Update();

		// Draw calls
		// Clear
		window_.clear();

		// Draw stuff
		connection_manager_.clients_mutex()->lock();
		DrawConnections(&window_, connection_manager_.clients(), &text_);
		connection_manager_.clients_mutex()->unlock();

		// Display
		window_.display();
	}

	connection_manager_.CleanUp();

	return 0;
}

// Print active connections to the window
void DrawConnections(sf::RenderWindow* window, list<NPS::ConnectionTCP*>* clients, sf::Text* text)
{
	// Heading
	text->setString("Connected clients: " + std::to_string(clients->size()));
	text->setPosition(0, 30);
	window->draw(*text);

	// Set spacing and position
	int text_spacing = 25;
	text->setPosition(0, (text->getPosition().y + (text_spacing)));
	
	// Write name of all connections in list to window
	list<NPS::ConnectionTCP*>::const_iterator it = clients->begin();
	for (int i = 0; i < clients->size(); ++i)
	{
		text->setString((*it)->name());
		text->setPosition(0, (text->getPosition().y + text_spacing));
		window->draw(*text);
		++it;
	}
}