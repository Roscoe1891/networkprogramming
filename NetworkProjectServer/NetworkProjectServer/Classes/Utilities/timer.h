#ifndef TIMER_H
#define TIMER_H

#include <SFML\System.hpp>

namespace NPS
{
	class Timer
	{
	public:
		Timer();
		~Timer();
		void Update();

		sf::Int64 DeltaTime();

	private:
		void ResetGameTime();

		sf::Clock clock_;
		sf::Time old_time_;
		sf::Time delta_time_;

	};
}



#endif // !TIMER_H
