#include "Utilities\timer.h"

namespace NPS
{
	Timer::Timer()
	{
		old_time_ = sf::microseconds(0);
		delta_time_ = sf::microseconds(0);
	}

	Timer::~Timer()
	{
	}

	void Timer::Update()
	{
		delta_time_ = clock_.getElapsedTime() - old_time_;
		old_time_ = clock_.getElapsedTime();
	}

	sf::Int64 Timer::DeltaTime()
	{
		return delta_time_.asMicroseconds();
	}

	void Timer::ResetGameTime()
	{
		clock_.restart();
		old_time_ = sf::microseconds(0);
	}
}
