#include "connection_manager.h"

namespace NPS
{
	ConnectionManager::ConnectionManager() : update_thread_(nullptr)
	{
	}

	ConnectionManager::~ConnectionManager()
	{
	}

	void ConnectionManager::Init()
	{
		listening_mutex_.lock();
		listening_ = true;
		listening_mutex_.unlock();

		// bind listener to server port
		if (listener_.listen(SERVER_PORT) != sf::Socket::Done)
		{
			std::cout << "Server listen call was unsuccessful." << std::endl;
		}
		std::cout << "Server is listening to port " << SERVER_PORT << ", waiting for connections... " << std::endl;

		// add listener to selector
		selector_.add(listener_);

		// start thread to run update function
		update_thread_ = new thread(&ConnectionManager::Update, this);
	}

	void ConnectionManager::Update()
	{
		while (listening_)
		{
			// Make selector wait on data from any socket
			if (selector_.wait(sf::milliseconds(SELECTOR_TIMEOUT)))
			{
				// if listener is ready
				if (selector_.isReady(listener_))
				{
					// Create a new client socket
					NPS::ConnectionTCP* new_connection = new NPS::ConnectionTCP;

					// If connection was successful
					if (listener_.accept(new_connection->socket()) == sf::Socket::Done)
					{
						// Add new connection
						clients_mutex_.lock();
						Connect(new_connection);
						clients_mutex_.unlock();

						selector_.add(new_connection->socket());
					}
					// if unsuccessful
					else
					{
						std::cout << "There was a problem accepting the new connection." << std::endl;
						delete new_connection;
						new_connection = nullptr;
					}
				}
				// if listener is not ready, check other sockets
				else
				{
					for (list<ConnectionTCP*>::iterator it = clients_.begin(); it != clients_.end();)
					{
						sf::TcpSocket& client_socket = (*it)->socket();
						if (selector_.isReady(client_socket))
						{
							// Print out the client's message
							string response = (*it)->ReceiveMessage();
							std::cout << "New Message from client: " << response << "." << std::endl;
							// If message read 'disconnect'
							if (response.compare(string("Disconnected")) == 0)
							{
								Disconnect(it);
							}
							// Or if message read 'error'
							else if (response.compare(string("Error")) == 0)
							{
								Disconnect(it);
							}
							// otherwise iterate
							else
							{
								++it;
							}
						}
						else
						{
							++it;
						}
					}
				}
				// Check number of unmatched players
				int unmatched = UnmatchedClients();
				std::cout << "Number of unmatched players is: " << unmatched << std::endl;
				if (unmatched >= USERS_PER_GROUP)
				{
					FormNewGroup();
				}
			}
		}
	}
	
	// Param[in] new connection to be set up
	void ConnectionManager::Connect(NPS::ConnectionTCP* connection)
	{
		// Inform when client connected
		std::cout << "Client connected: " << connection->socket().getRemoteAddress() << std::endl;

		// Set the client's name via receiving from the new connection
		string response = connection->ReceiveMessage();
		std::cout << "Response from client: " << response << std::endl;
		connection->set_name(response);

		// Send a message to the client via the new connection
		string welcome = "Welcome, " + response + ".";
		connection->SendMessage(welcome);

		// Add new connection 
		clients_.push_back(connection);
		std::cout << "Number of clients connected: " << clients_.size() << std::endl;
	}

	// Param[in] iterator to connection to be disconnected
	void ConnectionManager::Disconnect(list<ConnectionTCP*>::iterator& it)
	{
		std::cout << "Disconnecting client..." << std::endl;

		// send a confirmation message
		(*it)->SendMessage(string("Good-bye from the server"));

		// remove from selector
		selector_.remove((*it)->socket());

		// delete connection
		NPS::ConnectionTCP* conn = (*it);
		clients_mutex_.lock();
		it = clients_.erase(it);
		clients_mutex_.unlock();
		delete conn;
		conn = nullptr;
		std::cout << "Disconnected client." << std::endl;
	}

	int ConnectionManager::UnmatchedClients()
	{
		int result = 0;
		for (auto it = clients_.begin(); it != clients_.end(); ++it)
		{
			if (!(*it)->matched())
				result++;
		}
		return result;
	}

	void ConnectionManager::FormNewGroup()
	{
		std::cout << "Forming a new group." << std::endl;
		// Create a new group
		CreateUserGroup();
		// Print all groups
		PrintLatestUserGroup();
		// Appoint Host
		ConnectionTCP* host = AppointHost(user_groups_.back());
		std::cout << "Host for this game is: " << host->name() << std::endl;
		// Inform User Group
		InformGroup(user_groups_.back(), host);
	}

	void ConnectionManager::CreateUserGroup()
	{
		ConnectionTCP** new_user_group = new UserGroup;

		int group_index = 0;
		for (auto it = clients_.begin(); it != clients_.end(); ++it)
		{
			// if 4 users have been added, we have enough (break from loop)
			if (group_index == USERS_PER_GROUP)
				break;

			// if current client has not been matched, add to group
			if (!(*it)->matched())
			{
				new_user_group[group_index] = *it;
				(*it)->set_matched(true);
				group_index++;
			}
		}

		user_groups_.push_back(new_user_group);
	}

	// Prints users in each group to COut
	void ConnectionManager::PrintLatestUserGroup()
	{
		if (!user_groups_.empty())
		{
			int group_index = user_groups_.size() - 1;
			ConnectionTCP** user_group = user_groups_.back();
			
			std::cout << "Users in Group " << group_index << ":" << std::endl;
			for (int i = 0; i < USERS_PER_GROUP; ++i)
			{
				std::cout << user_group[i]->name() << std::endl;
			}
		}
		else std::cout << "Warning: Trying to print latest user group when no groups exist!" << std::endl;
	}

	// Param[in] group from which to appoint host, returning pointer to appointed host connection
	ConnectionTCP* ConnectionManager::AppointHost(ConnectionTCP** group)
	{
		ConnectionTCP* host = nullptr;
		
		host = group[USERS_PER_GROUP - 1];
		host->set_host(true);

		return host;
	}

	void ConnectionManager::InformGroup(ConnectionTCP** group, ConnectionTCP* host)
	{
		// get host IP address
		sf::IpAddress host_addr = host->socket().getRemoteAddress();
		string host_addr_string = host_addr.toString();

		for (sf::Int8 i = 0; i < USERS_PER_GROUP; ++i)
		{
			string message = "";
			char p_id;
			switch(i)
			{
			case 0:
				p_id = '0';
				break;
			case 1:
				p_id = '1';
				break;
			case 2:
				p_id = '2';
				break;
			case 3:
				p_id = '3';
				break;
			}

			message += p_id;

			if (group[i] == host)
			{
				message += "You are the host.";
				sf::IpAddress client_addr;
				string client_addr_string = "";
				for (int j = 0; j < USERS_PER_GROUP; ++j)
				{
					if (j != i)
					{
						// add address to string
						client_addr = group[i]->socket().getRemoteAddress();
						client_addr_string = client_addr.toString();
						message += (client_addr_string + ',');
					}
				}
			}
			else
			{
				message += "Host IP:";
				message += host_addr_string;
			}
			group[i]->SendMessage(message);

		}
	}

	void ConnectionManager::CleanUp()
	{
		// No longer listening
		listening_mutex_.lock();
		listening_ = false;
		listening_mutex_.unlock();

		// Close update thread
		if (update_thread_->joinable())
		{
			update_thread_->join();
		}

		delete update_thread_;
		update_thread_ = nullptr;

		// empty user groups
		DestroyAllGroups();
		// close all connections
		DisconnectAll();
	}

	void ConnectionManager::DestroyAllGroups()
	{
		ConnectionTCP** group = nullptr;
		for (auto it = user_groups_.begin(); it != user_groups_.end();)
		{
			group = (*it);
			it = user_groups_.erase(it);
			delete group;
			group = nullptr;
		}
	}

	void ConnectionManager::DisconnectAll()
	{
		for (auto it = clients_.begin(); it != clients_.end();)
		{
			Disconnect(it);
		}
	}


}