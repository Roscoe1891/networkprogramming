#ifndef _CONNECTION_MANAGER_H
#define _CONNECTION_MANAGER_H

#include <list>
#include <mutex>
#include <thread>
#include "connection_tcp.h"

// The TCP port number for the server to listen on
#define SERVER_PORT 5555
// Port to contact host on 
#define HOST_PORT 5556
// Number of users per user group
#define USERS_PER_GROUP 2
// Time(ms) after which to abandon network operations 
#define SELECTOR_TIMEOUT 2000

typedef NPS::ConnectionTCP* UserGroup[USERS_PER_GROUP];

using std::list;
using std::mutex;
using std::thread;

namespace NPS
{
	class ConnectionManager
	{
	public:
		ConnectionManager();
		~ConnectionManager();

		// return pointer to clients list
		inline list<NPS::ConnectionTCP*>* clients() { return &clients_; }

		// return clients mutex
		inline mutex* clients_mutex() { return &clients_mutex_; }

		void Init();
		void Update();
		void CleanUp();

	private:
		// Param[in] new connection
		void Connect(NPS::ConnectionTCP* connection);
		// Param[in] iterator to connection to be disconnected
		void Disconnect(list<ConnectionTCP*>::iterator& it);
		// Disconnects all connections
		void DisconnectAll();
		// Returns number of clients (i.e. connections) which have not been matched
		int UnmatchedClients();
		
		// Form New Group
		void FormNewGroup();
		
		// Creates a user group
		void CreateUserGroup();
		// Empty and destroy all groups
		void DestroyAllGroups();

		// Prints last connected user group to cout
		void PrintLatestUserGroup();

		// Param[in] group from which to appoint host, returning pointer to appointed host connection
		ConnectionTCP* AppointHost(ConnectionTCP** group);

		void InformGroup(ConnectionTCP** group, ConnectionTCP* host);

		// A dedicated thread for updating the connection manager
		thread* update_thread_;

		// A dedicated socket to listen for connections
		sf::TcpListener listener_;

		// list to hold connected clients
		list<NPS::ConnectionTCP*> clients_;
		mutex clients_mutex_;

		// list to hold user groups
		list<ConnectionTCP**> user_groups_;

		// Create a selector to switch between sockets
		sf::SocketSelector selector_;

		// true if listening for new connections
		bool listening_;
		mutex listening_mutex_;
		
	};
}




#endif
