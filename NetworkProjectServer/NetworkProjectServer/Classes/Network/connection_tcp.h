#ifndef _CONNECTION_TCP_H
#define _CONNECTION_TCP_H

#include <iostream>
#include <SFML\Network.hpp>
#include <vector>
#include <string>

// The maximum message size
#define MAX_MESSAGE_SIZE 97

using std::vector;
using std::string;

// The connection class represents a connection with a client user

namespace NPS
{
	class ConnectionTCP
	{
	public:
		ConnectionTCP();
		~ConnectionTCP();

		// return reference to this connection's socket
		inline sf::TcpSocket& socket()  { return socket_; }
		// return reference to this connection's name
		inline string& name() { return name_; }
		// param[in] new name for this connection
		inline void set_name(string& connection_name) { name_ = connection_name; }
		// return reference to matched
		inline const bool& matched() const { return matched_; }
		// param[in] new value of matched
		inline void set_matched(bool new_value) { matched_ = new_value; }
		// return reference to host
		inline const bool& host() const { return host_; }
		// param[in] new value of host
		inline void set_host(bool new_value) { host_ = new_value; }

		// Param[in] string to send as a message across this connection
		bool SendMessage(string& message);
		// Returns string received as a message across this connection
		string ReceiveMessage();

	private:
		// Send Utilities
		string BuildMessage(string& message);
		
		// Receive utilities
		uint16_t GetMessageSize();
		string ReadFromBuffer(string& message);

		vector<char> buffer_;
		sf::TcpSocket socket_;
		string name_;			// user name associated with this connection
		bool matched_;			// true if this connection has been matched to a game
		bool host_;				// true if this connection represents a host
	};
}

#endif // !_CONNECTION_H
